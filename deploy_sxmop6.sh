#!/bin/sh
# Description: deploy sxmop6
# Author: magdesign
# Note: v0.21

# shellcheck source=scripts/core/sxmo_common.sh
#. "/usr/bin/sxmo_common.sh"

# titel
echo -e "\e[31msxmop6 deploy script v0.9 for op6/pocof1 (needs internet!)\e[0m"

# sudo check
if [ $(id -u) -eq 0 ]; then
	echo -e "\e[31mDont run with sudo, it will ask when sudo is needed\e[0m"
    exit 1
fi

# distro check
if cat /etc/os-release | grep -q 'NAME="postmarketOS"'>/dev/null 2>&1 ;then
	echo -e "\033[34mCool, you run PostmarketOS\033[0m"
else
	echo -e "\e[31mWrong distro!\e[0m"
  exit 1
fi
# sxmo check
if [ -f /usr/bin/sxmo_version.sh ]; then
	echo -e "\033[34mNice, you run sxmo\033[0m"

else
	echo -e "\e[31mNo sxmo installed, aborting...\e[0m"
  exit 1
fi

echo -e "\033[34mPre-checks finished...\033[0m"



echo -e "\e[1;32m---------------------------\e[0m"
echo -e "\e[1;32m| Userscripts             |\e[0m"
echo -e "\e[1;32m---------------------------\e[0m"

# add display scaling
echo -e "\e[1;32mAdd a display scaling entry to scripts[Y/n]\e[0m"
read deploy_scaling
if [ "$deploy_scaling" == "n" ]; then
    echo "wow, you never need scaling, respect!"
else
	mkdir -p ~/.config/sxmo/userscripts
	cp userscripts/sxmo_scaledisplay.sh ~/.config/sxmo/userscripts/
	sudo chmod +x  ~/.config/sxmo/userscripts/sxmo_scaledisplay.sh
	echo "there is now a nice display scale in System Menu/Scripts"
fi

echo -e "\e[1;32mAdd a qr/barcode reader and screenreading script to scripts[Y/n]\e[0m"
read deploy_ocr
if [ "$deploy_ocr" == "n" ]; then
    echo "no screenreader to clipboard script installed"
else
	mkdir -p ~/.config/sxmo/userscripts
	sudo apk add tesseract-ocr tesseract-ocr-data-eng tesseract-ocr-data-rus wl-clipboard zxing
	cp userscripts/ocr_roi.sh ~/.config/sxmo/userscripts/
	sudo chmod +x  ~/.config/sxmo/userscripts/ocr_roi.sh
	echo "there is now a nice text to clipboard incl. barcode and qr reader in scripts"
fi

# weather
echo -e "\e[1;32mEnable worldwide weather forecast [Y/n]\e[0m"
read deploy_weather
if [ "$deploy_weather" == "n" ]; then
    echo "you get only usa weather"
else
	sudo cp bin/sxmo_weather.sh /usr/bin/
	sudo chmod +x /usr/bin/sxmo_weather.sh
	echo "worldwide weather installed"
fi

# audio recorder
echo -e "\e[1;32mInstall a working Audio Recorder to Userscripts [Y/n]\e[0m"
read deploy_audiorecorder
if [ "$deploy_audiorecorder" == "n" ]; then
    echo "the default Recorder is broken!"
else
	sudo cp bin/sxmo_record.sh /usr/bin/
	sudo chmod +x /usr/bin/sxmo_record.sh
	echo "working Audio Recorder installed"
fi


# temperature
echo -e "\e[1;32mTemperature checking/watching script to userscripts [Y/n]\e[0m"
read deploy_temperature
if [ "$deploy_temperature" == "n" ]; then
    echo "no temperature watching script"
else
	mkdir -p ~/.config/sxmo/userscripts
	cp userscripts/temperaturewatcher.sh ~/.config/sxmo/userscripts/
	sudo chmod +x  ~/.config/sxmo/userscripts/temperaturewatcher.sh
	echo "temperaturewatcher in scripts"
fi

# night-mode
echo -e "\e[1;32mAdd nightmode to userscripts for very low screen brightness [Y/n]\e[0m"
read deploy_night
if [ "$deploy_night" == "n" ]; then
    echo "no night setting"
else
	sudo cp userscripts/night_mode.sh ~/.config/sxmo/userscripts/night_mode.sh
	sudo chmod +x ~/.config/sxmo/userscripts/night_mode.sh
	echo "nightmode script installed"
fi

# gps-enable
echo -e "\e[1;32mAdd GPS enabler entry to scripts[Y/n]\e[0m"
read deploy_gpsscript
if [ "$deploy_gpsscript" == "n" ]; then
    echo "no gps enabler"
else
	mkdir -p ~/.config/sxmo/userscripts
	sudo cp userscripts/gps_enable.sh ~/.config/sxmo/userscripts/gps_enable.sh
	sudo chmod +x ~/.config/sxmo/userscripts/gps_enable.sh
	echo "GPS enabler script installed"
fi

# openwifi
echo -e "\e[1;32mShow open wifi networks script [Y/n]\e[0m"
read deploy_openwifi
if [ "$deploy_openwifi" == "n" ]; then
    echo "i know, this should be in networks menu"
else
	mkdir -p ~/.config/sxmo/userscripts
	sudo cp userscripts/openwifi.sh ~/.config/sxmo/userscripts/openwifi.sh
	sudo chmod +x ~/.config/sxmo/userscripts/openwifi.sh
	echo "display the unlocked wifis"
fi

# mini keyboard
echo -e "\e[1;32mInstall custom miniKeyboard [Y/n]\e[0m"
read deploy_minikeyboard
if [ "$deploy_minikeyboard" == "n" ]; then
    echo "no minikbd"
else
	sudo cp bin/wvkbd-mini /usr/local/bin/wvkbd-mini 
	sudo chmod +x /usr/local/bin/wvkbd-mini
	sudo mkdir -p ~/.config/sxmo/userscripts/
	sudo cp userscripts/minikeybord_toggle.sh ~/.config/sxmo/userscripts/minikeybord_toggle.sh
	sudo chmod +x ~/.config/sxmo/userscripts/minikeyboard_toggle.sh	
	echo "installed custom mini wvkbd in /usr/local/bin and toggler in userscripts"
fi

# yt music dl
echo -e "\e[1;32mInstall Youtube Music Downloader [Y/n]\e[0m"
read deploy_yt
if [ "$deploy_yt" == "n" ]; then
    echo "no YT downloader"
else
	sudo mkdir -p ~/.config/sxmo/userscripts/
	sudo cp userscripts/sxmo_youtube2audio.sh  ~/.config/sxmo/userscripts/sxmo_youtube2audio.sh 
	sudo chmod +x ~/.config/sxmo/userscripts/sxmo_youtube2audio.sh p	
	echo "installed Youtube2audio downloader in  userscripts"
fi



echo -e "\e[1;32m---------------------------\e[0m"
echo -e "\e[1;32m| Systemwide              |\e[0m"
echo -e "\e[1;32m---------------------------\e[0m"


# mac randomisation
echo -e "\e[1;32mEnable MAC randomisation with removing 00-sxmo.conf [Y/n]\e[0m"
read deploy_macrandom
if [ "$deploy_macrandom" == "n" ]; then
    echo "left 00-sxmo.conf untouched"
else
        sudo mv /etc/NetworkManager/conf.d/00-sxmo.conf /etc/NetworkManager/conf.d/00-sxmo.conf.bak
        echo "00-sxmo.conf is moved to 00-sxmo.conf.bak, you can always restore"
fi
# audio
#echo -e "\e[1;32mApply audio crackling fix (pipewire only): https://gitlab.com/sdm845-mainline/linux/-/issues/48? [y/N] \e[0m"
#read deploy_audiofix
#if [ "$deploy_audiofix" == "y" ]; then
#	if command -v pipewire-tools >/dev/null 2>&1 ;then
#	echo "pipewire-tools are installed"
#	else
#	  echo "installing pipewire-tools"
#	  sudo apk add pipewire-tools
#	fi
#	sudo rm -r /etc/wireplumber/main.lua.d
#	echo "removed main.lua.d..."
#	sudo mkdir -p /etc/wireplumber/wireplumber.conf.d/
#	sudo cp configs/51-disable-suspension.conf /etc/wireplumber/wireplumber.conf.d/51-disable-suspension.conf
#	echo "disable-suspension.conf created..."
#	sudo cp configs/call_audio_idle_suspend_workaround /usr/sbin/call_audio_idle_suspend_workaround
#	sudo chmod +x /usr/sbin/call_audio_idle_suspend_workaround
#   echo "all known stuff to fix audio applied"
#else
#    echo "audio fix skipped."
#fi

# audio suspend
echo -e "\e[1;32mPrevent system to suspend when listening music with adding playerctl[y/N]\e[0m"
read deploy_audiosuspendfix
if [ "$deploy_audiosuspendfix" == "y" ]; then
#	mkdir -p ~/.config/sxmo/hooks/
#	cp hooks/sxmo_hook_block_suspend.sh ~/.config/sxmo/hooks/sxmo_hook_block_suspend.sh
#	sudo chmod +x ~/.config/sxmo/hooks/sxmo_hook_block_suspend.sh
#	echo "done.. since there is no 'playerctl' by default, so we check for running audio streams"
	sudo apk add playerctl
	echo "playerctl added"
else
    echo "audiosuspend fix skipped."
fi
# battery 
echo -e "\e[1;32mWarn on low power and shutdown as in UPower.conf defined (op6 only!)[Y/n]\e[0m"
read deploy_battery
if [ "$deploy_battery" == "n" ]; then
    echo "no changes made your phone dies without warning, without proper shutdown"
else
	mkdir -p ~/.config/sxmo/hooks/
	cp hooks/sxmo_hook_battery.sh ~/.config/sxmo/hooks/
	sudo chmod +x ~/.config/sxmo/hooks/sxmo_hook_battery.sh
	echo "there will be a warning before your phone will shutdown on low power, please enable also LEDs"
fi

# battery 
echo -e "\e[1;32mWarn on low power and shutdown as in UPower.conf defined (pocof1 only!)[Y/n]\e[0m"
read deploy_batterypoco
if [ "$deploy_batterypoco" == "n" ]; then
    echo "no changes made your phone dies without warning, without proper shutdown"
else
	mkdir -p ~/.config/sxmo/hooks/
	cp hooks/sxmo_hook_battery_poco.sh ~/.config/sxmo/hooks/sxmo_hook_battery.sh
	sudo chmod +x ~/.config/sxmo/hooks/sxmo_hook_battery.sh
	echo "there will be a warning before your phone will shutdown on low power, please enable also LEDs"
fi


# noatime 
echo -e "\e[1;32mPrevent writing file access time to disk (noatime), helps performance & emmc wearout [Y/n]\e[0m"
read deploy_noatime
if [ "$deploy_noatime" == "n" ]; then
    echo "no changes made in /etc/fstab"
else
	if  grep -q "defaults,noatime" /etc/fstab; then
		echo "noatime is already there"
	else
		sudo sed -i 's/defaults/defaults,noatime/g' /etc/fstab
		echo "wrote noatime to /etc/fstab"
	fi
fi

# darkmode
echo -e "\e[1;32mApply darkmode (needs internet!) [Y/n]\e[0m"
read deploy_darkmode
if [ "$deploy_darkmode" == "n" ]; then
    echo "darkmode skipped..."
else
	gsettings set org.gnome.desktop.interface color-scheme prefer-dark 
    echo "gsettings for darkmode applied"
    echo "download kde apps darknode.."
    sudo apk add plasma-integration qqc2-breeze-style
	echo "copy the file.."
	cp configs/kdeglobals ~/.config 
	QT_QUICK_CONTROLS_STYLE=org.kde.breeze QT_QPA_PLATFORMTHEME=kde
	echo "kde darkmodes applied"
	sudo flatpak override --env=GTK_THEME=Adwaita:dark
	flatpak override --user --env=GTK_THEME=Adwaita:dark
fi

# conky version
#echo -e "\e[1;32mDowngrade conky to latest workable version 1.19.6 [Y/n]\e[0m"
#read deploy_conkyvers
#if [ "$deploy_conkyvers" == "n" ]; then
#    echo "keep the broken one and have no clock on homescreen"
#else        
#	wget https://dl-cdn.alpinelinux.org/alpine/v3.19/main/aarch64/conky-1.19.6-r0.apk
#	sudo apk add conky-1.19.6-r0.apk | echo Y
#	echo "conky downgraded"
#fi
# custom conky.conf
echo -e "\e[1;32m[op6]Customized conky file to show more info on desktop[Y/n]\e[0m"
read deploy_conkyop6
if [ "$deploy_conkyop6" == "n" ]; then
    echo "keep the default time&date"
else
	sudo cp configs/op6.conky.conf ~/.config/sxmo/
	echo "added more usful info to desktop"
fi
# custom conky.conf
echo -e "\e[1;32m[pocof1]Customized conky file to show more info on desktop[Y/n]\e[0m"
read deploy_conky
if [ "$deploy_conky" == "n" ]; then
    echo "keep the default time&date"
else
	sudo cp configs/conky.conf ~/.config/sxmo/
	echo "added more usful info to desktop"
fi


# vnstat
echo -e "\e[1;32mShow data usage on homescreen [Y/n]\e[0m"
read deploy_vnstat
if [ "$deploy_vnstat" == "n" ]; then
    echo "nothing touched"
else
    sudo apk add vnstat
	sudo rc-update add vnstatd default
	sudo rc-service vnstatd start
	mkdir -p $HOME/.config/sxmo/userscripts/
	sudo chmod +x userscripts/datausage_reset.sh
	cp userscripts/datausage_reset.sh $HOME/.config/sxmo/userscripts/
	echo "added vnstat, enable service on boot, added datausage reset userscripts to scripts"
fi

# network menu
echo -e "\e[1;32mAdd custom Networks menu entry with: ssh&gsm toggle, no duplicated wifis, show if its open or wpa, show credentials and fixed hotspot creation bug? [Y/n]\e[0m"
read deploy_networkmenu
if [ "$deploy_networkmenu" == "n" ]; then
    echo "keeping standard menu"
else
	sudo cp bin/sxmo_networks.sh /usr/bin/
	sudo chmod +x /usr/bin/sxmo_networks.sh
	# adding ssh toggle script, for doas
	sudo cp bin/sxmo_sshtoggle.sh /usr/bin/
	sudo chmod +x /usr/bin/sxmo_sshtoggle.sh
	echo "Networks menu just got expanded"
fi

# context menu
echo -e "\e[1;32mAdd System Menu with: removed Binaries, better filebrowser contextmenu: copy sms, firefox delete cookies [Y/n]\e[0m"
read deploy_contextmenu
if [ "$deploy_contextmenu" == "n" ]; then
    echo "keeping standard menu"
else
	mkdir -p ~/.config/sxmo/hooks/
	cp hooks/sxmo_hook_contextmenu.sh ~/.config/sxmo/hooks/
	sudo chmod +x  ~/.config/sxmo/hooks/sxmo_hook_contextmenu.sh
	sudo apk add yazi
	echo "Binaries entry removed, spf for files, contextmenues to remove firefox cookies added"
fi

echo -e "\e[1;32m--------------------------------\e[0m"
echo -e "\e[1;32m| OP6/pocof1 only things coming  |\e[0m"
echo -e "\e[1;32m--------------------------------\e[0m"

# status bar, notch
echo -e "\e[1;32m[op6]Work around the notch hack[Y/n]\e[0m"
read deploy_statusbar
if [ "$deploy_statusbar" == "n" ]; then
    echo "keeping stuff hidden by the notch"
else
	mkdir -p ~/.config/sxmo/hooks/
	cp hooks/sxmo_hook_statusbar.sh ~/.config/sxmo/hooks/
	sudo chmod +x  ~/.config/sxmo/hooks/sxmo_hook_statusbar.sh
	echo "filled stuff around the notch"
fi


# status bar, notch pocof1
echo -e "\e[1;32m[poco1]Work around the notch and hide clock[Y/n]\e[0m"
read deploy_statusbarf1
if [ "$deploy_statusbarf1" == "n" ]; then
    echo "keeping stuff hidden by the notch"
else
	mkdir -p ~/.config/sxmo/hooks/
	cp hooks/sxmo_hook_statusbar_poco.sh ~/.config/sxmo/hooks/
	sudo chmod +x  ~/.config/sxmo/hooks/sxmo_hook_statusbar.sh
	echo "filled stuff around the notch, removed clock"
fi



# fix notification led
# https://git.sr.ht/~fdlamotte/sxmo-personalscripts/tree/master/item/scripts/sxmo_led.sh
echo -e "\e[1;32m[op6]Fix notification LED[Y/n]\e[0m"
read deploy_notification
if [ "$deploy_notification" == "n" ]; then
    echo "notification led stays unused"
else
	sudo cp rules/99-custom-led-permissions.rules /etc/udev/rules.d/99-custom-led-permissions.rules
	echo "rules added"
	sudo cp bin/sxmo_led.sh /usr/bin/ 
	sudo chmod +x /usr/bin/sxmo_led.sh
	echo "notification led enabled"
fi

# enable sensors & rotation:
echo -e "\e[1;32m[op6&pocof1]Enable sensors like rotation + proximity[Y/n]\e[0m"
read deploy_sensors
if [ "$deploy_sensors" == "n" ]; then
    echo "no sensors installed"
else
	echo "install sensors:"
	sudo apk add iio-sensor-proxy
	sudo rc-update add iio-sensor-proxy default
# see: https://lists.sr.ht/~mil/sxmo-devel/patches/51394
	echo "add rules.."
	sudo cp rules/01-sensor-claim.rules /etc/polkit-1/rules.d/01-sensor-claim.rules 
	sudo cp bin/sxmo_autorotate.sh /usr/bin/
	sudo chmod +x /usr/bin/sxmo_autorotate.sh
	echo "autorotation works now"
# proximity
	sudo cp bin/sxmo_proximitylock.sh /usr/bin/sxmo_proximitylock.sh
	sudo chmod +x /usr/bin/sxmo_proximitylock.sh
	echo "proximitylock should work now"
fi


echo -e "\e[1;32m---------------------------\e[0m"
echo -e "\e[1;32m| Application Settings    |\e[0m"
echo -e "\e[1;32m---------------------------\e[0m"

# firefox policies, ublock rules
echo -e "\e[1;32mAdd Firefox customized policies[Y/n]\e[0m"
read deploy_policies
if [ "$deploy_policies" == "n" ]; then
    echo "leave as is"
else
	sudo cp configs/policies.json /etc/firefox/policies.json
	echo "added policies, please manually add the ublock rules from configs folder"
fi


echo -e "\e[1;32m---------------------------\e[0m"
echo -e "\e[1;32m| Hardware                |\e[0m"
echo -e "\e[1;32m---------------------------\e[0m"

# add camera support
echo -e "\e[1;32m[pocof1]Add rules to enable camera[Y/n]\e[0m"
read deploy_camera
if [ "$deploy_camera" == "n" ]; then
    echo "no camera then..."
else
	cp rules/90-camera.rules /etc/udev/rules.d/90-camera.rules
	sudo chmod 777 /dev/udmabuf
	sudo chmod 777 /dev/urandom
	echo "after a reboot you have a working camera... install 'snapshot' to test it"
fi


echo "here i should add NFC enabling"



# query slider
echo -e "\e[1;32m[op6]Add testsw tool to query slider position[Y/n]\e[0m"
read deploy_slider
if [ "$deploy_slider" == "n" ]; then
    echo "leave as is"
else
	sudo cp bin/testsw /usr/bin/testsw
	sudo chmod +x /usr/bin/testsw
	echo "added testsw to /usr/bin"
fi

echo -e "\e[1;32m---------------------------\e[0m"
echo -e "\e[1;32m| following only works with 'testsw'    |\e[0m"
echo -e "\e[1;32m---------------------------\e[0m"

echo -e "\e[1;32m[op6]Slider down locks phone[Y/n]\e[0m"
read deploy_locker
if [ "$deploy_locker" == "n" ]; then
    echo "leave as is"
else
	sudo mkdir -p ~/.config/sxmo/hooks/
	sudo cp hooks/sxmo_hook_inputhandler.sh.op6 ~/.config/sxmo/hooks/sxmo_hook_inputhandler.sh
	sudo chmod +x ~/.config/sxmo/hooks/sxmo_hook_inputhandler.sh
	echo "enabled phonelock when slider is in down position"
fi

echo -e "\e[1;32m[op6]Slider up prevents suspend/sleep [Y/n]\e[0m"
read deploy_suspend
if [ "$deploy_suspend" == "n" ]; then
    echo "leave as is"
else
sudo mkdir -p ~/.config/sxmo/hooks/
	sudo cp hooks/sxmo_hook_block_suspend.sh  ~/.config/sxmo/hooks/sxmo_hook_block_suspend.sh 
	sudo chmod +x ~/.config/sxmo/hooks/sxmo_hook_block_suspend.sh 
	echo "enabled suspend prevent when slider is in up position"
fi


echo -e "\e[1;32m----------------------------------\e[0m"
echo -e "\e[1;32m| Binary Things, more Complicated   |\e[0m"
echo -e "\e[1;32m----------------------------------\e[0m"


echo -e "\e[1;32m[op6]Install TouchpadEmulator to simulate a mouse (modifies uinput permissions!)[Y/n]\e[0m"
read deploy_touchpad
if [ "$deploy_touchpad" == "n" ]; then
    echo "leave as is"
else
	sudo cp bin/TouchpadEmulator /usr/bin/TouchpadEmulator 
	sudo chmod +x /usr/bin/TouchpadEmulator 
	sudo modprobe uinput
	sudo chmod 777 /dev/uinput
	echo "installed patched TouchpadEmulator by Adam Honse"
fi

echo -e "\e[1;32m[pocof1(tianma)]Install TouchpadEmulator to simulate a mouse (modifies uinput permissions!)[Y/n]\e[0m"
read deploy_touchpadpoco
if [ "$deploy_touchpadpoco" == "n" ]; then
    echo "leave as is"
else
	sudo cp bin/TouchpadEmulator_poco /usr/bin/TouchpadEmulator 
	sudo chmod +x /usr/bin/TouchpadEmulator 
	sudo modprobe uinput
	sudo chmod 777 /dev/uinput
	echo "installed patched TouchpadEmulator by Adam Honse"
fi


echo -e "\e[1;32mInstall custom Keyboard with other emojis, ° symbol, and custom section to trigger touchpademulator and ocr scripts[Y/n]\e[0m"
read deploy_keyboard
if [ "$deploy_keyboard" == "n" ]; then
    echo "leave as is"
else
	sudo cp bin/wvkbd-mobintl /usr/local/bin/wvkbd-mobintl 
	sudo chmod +x /usr/local/bin/wvkbd-mobintl 
	echo "installed custom wvkbd, click 'Cmp + Space' to be able to trigger ocr and touchpad also install custom sway config"
fi

echo -e "\e[1;32mInstall custom sway config to harmony with new keyboard buttons[Y/n]\e[0m"
read deploy_swayconf
if [ "$deploy_swayconf" == "n" ]; then
    echo "leave as is"
else
	cp ~/.config/sxmo/sway ~/.config/sxmo/sway.bak
	cp configs/sway ~/.config/sxmo/sway
	sudo swaymsg reload
	echo "installed custom sway, enjoy new keyboard functions (the original sway is backuped =)"
fi

# sway config for my custom keyboard mod with sed instead of overwrite full file:
#sed -i 's/bindsym $mod+5 workspace number 5/bindsym $mod+5 exec ~\/sxmop6\/launchers\/texttospeech_en_launcher.sh/' ~/.config/sxmo/sway
#sed -i 's/bindsym $mod+6 workspace number 6/bindsym $mod+6 exec ~\/sxmop6\/launchers\/texttospeech_de_launcher.sh/' ~/.config/sxmo/sway
#sed -i 's/bindsym $mod+7 workspace number 7/bindsym $mod+7 exec ~\/sxmop6\/launchers\/touchpademulator_launcher.sh/' ~/.config/sxmo/sway
#sed -i 's/bindsym $mod+8 workspace number 8/bindsym $mod+8 exec ~\/sxmop6\/launchers\/dictation_launcher.sh/' ~/.config/sxmo/sway
#sed -i 's/bindsym $mod+9 workspace number 9/bindsym $mod+9 exec ~\/sxmop6\/userscripts\/ocr_roi.sh/' ~/.config/sxmo/sway
#sed -i 's/bindsym $mod+0 workspace number 10/bindsym $mod+0 exec ~\/sxmop6\/launchers\/texttospeech_launcher.sh/' ~/.config/sxmo/sway
#swaymsg reload




# install krita and its mobile layout
# kdenlive and mobile layout


# install spf

# install podman and libre translator

# offline speech stuff
# copy userscripts and dependencies
# remove reddit script
echo -e "\e[1;32m Finished customizing your sxmo, reboot sounds like a good idea!  feedback very welcome\e[0m"
