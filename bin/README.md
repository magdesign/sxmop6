# Bin's

Here you find executables which go into /usr/bin.

## sxmo_networks.sh 
version with ssh toggle, show wifi passwords, fixed hotspot bug and others...

## sxmo_sshtoggle.sh
needed to toggle ssh from sxmo_networks.sh via doas.

## sxmo_record.sh
a working audio recorder, since the one in sxmo 1.17.x is borked.
goes into: /usr/share/sxmo/appscripts/

## sxmo_timer.sh
a propper working timer, see instructions:
https://wiki.postmarketos.org/wiki/User:Magdesign#Alarm_Clock
goes into: /usr/share/sxmo/appscripts/

## lisgd
has optimized values of 100 for all edge gesture due to the massive notch.
goes to /usr/bin/lisgd.

## sxmo_weather.sh
a weather script which works for all locations.
goes into: /usr/share/sxmo/appscripts/

## snapshot v48.beta
patched aperture/src/utils.rs with full resolution support.
goes into /usr/bin

## testsw
tool to query op6 slider

## TouchpadEmulator
Fixed version of TouchpadEmulator for op6 on sxmo.
https://wiki.postmarketos.org/wiki/User:Magdesign#TouchpadEmulator

## TouchpadEmulator_poco
Fixed version of TouchpadEmulator for pocof1 on sxmo.
https://wiki.postmarketos.org/wiki/User:Magdesign#TouchpadEmulator

## wvkbd-mini
Mini Keyboard version for sxmo.

## wvkbd-mobintl
Customised Keyboard with the emojis i need.
https://wiki.postmarketos.org/wiki/User:Magdesign#Keyboard_Modifications
