#!/bin/sh
# title="$icon_wtr Weather"
# author: magdesign
# note: v0.1.1

# to do;
# add choice for right now or tomorrow
# curl wttr.in/oulu?format=4


HISTORY_FILE="$XDG_CACHE_HOME"/sxmo/weatherhistory.tsv

searchmenu() {
    HISTORY="$(
        tac "$HISTORY_FILE" | nl | sort -uk 2 | sort -k 1 | cut -f 2 | grep .
    )"

    MENU_OPTIONS="Abort
IP-Based
$HISTORY
Clear History"

    while true; do
        # Prompt for location input
        entry=$(echo -e "$MENU_OPTIONS" | /usr/bin/sxmo_dmenu.sh -p "Location?")

        if [ "$entry" = "Abort" ]; then
            break
        elif [ "$entry" = "Clear History" ]; then
            > "$HISTORY_FILE"
            notify-send "History cleared."
            break
        elif [ "$entry" = "IP-Based" ]; then
       		entry=""
        fi

        # Check if the entry is history or new 
        if echo "$entry" | grep -q "History: "; then
            # Extract the actual location from history
            entry=$(echo "$entry" )
        fi

        #if [ -n "$entry" ]; then
            weather=$(curl -s wttr.in/"$entry"?format=4)
			condition=$(curl -s wttr.in/"$entry"?format=%C)
			   moon=$(curl -s wttr.in/"$entry"?format=%m)
            notify-send "$weather" "condition: $condition | moon: $moon"
            
            # Append entry to history
            echo -e "$entry" >> "$HISTORY_FILE"
            break
#        fi
    done
}

searchmenu
