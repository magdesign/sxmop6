#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-only
# Copyright 2022 Sxmo Contributors

# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. sxmo_common.sh

# This hook enables the proximity lock.

finish() {
	sxmo_wakelock.sh unlock sxmo_proximity_lock_running

	kill $MONPID

	if [ -n "$INITIALSTATE" ]; then
		sxmo_state.sh set "$INITIALSTATE"
	fi

	exit
}

near() {
	if [ -z "$INITIALSTATE" ]; then
		INITIALSTATE="$(cat "$SXMO_STATE")"
	fi

	sxmo_debug "near"
	sxmo_state.sh set screenoff
}

far() {
	if [ -z "$INITIALSTATE" ]; then
		INITIALSTATE="$(cat "$SXMO_STATE")"
	fi

	sxmo_debug "far"
	sxmo_state.sh set unlock
}

trap 'finish' TERM INT

sxmo_wakelock.sh lock sxmo_proximity_lock_running infinite

monitor-sensor proximity | while read -r line; do
	if echo "$line" | grep -q ".*Proximity value.*1"; then
		near
	elif echo "$line" | grep -q ".*Proximity value.*0"; then
		far
	elif echo "$line" | grep -q "=== Has proximity.*1"; then
		near
	elif echo "$line" | grep -q "=== Has proximity.*0"; then
		far
	fi
done &
MONPID=$?

wait

finish
