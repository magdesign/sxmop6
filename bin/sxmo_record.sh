#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-only
# Copyright 2025 Sxmo Contributors
# title="$icon_mic Record Audio"
# Description: Record Audio from current alsa input
# Notes: v0.5
# needs arecord, ffmpeg

# todo: option to record call (mic+earpiece/systemsound)
# with a loopback device or ffmpeg ffmpeg -f pulse -i default -f alsa -i hw:0,1 -c:a libopus rec.opus

# create recording dir 
[ -z "$SXMO_RECDIR" ] && SXMO_RECDIR="$HOME"/Music/recordings
mkdir -p "$SXMO_RECDIR"

ffmpeg_check(){
	if ! command -v ffmpeg >/dev/null 2>&1; then
		notify-send "please install ffmpeg" "aborting.."
		exit 1
	fi
}

recordmenu() {
	while true; do
		NRECORDINGS="$(find "$SXMO_RECDIR" -type f | wc -l)"
		OPTIONS="$(cat <<-EOF
			Start wav recording
			Start opus recording
			
			($NRECORDINGS) Recordings
			Delete Recordings
			
			Audio Settings
			Close Menu
		EOF
		)"
		
		   OPTION=$(printf "%s" "$OPTIONS" | sxmo_dmenu.sh -p "Record Audio")


		case "$OPTION" in
			"Start wav recording")
				NOW="$(date '+%y%m%d_%H%M%S')"
				#FILE="$NOW"
				sxmo_terminal.sh arecord -vv --format=cd "$SXMO_RECDIR/$NOW.wav" &
				ARECORD_PID=$!

				# dmenu dialog for stopping the recording
				while kill -0 $ARECORD_PID 2>/dev/null; do
					STOP_OPTION="$(sxmo_dmenu.sh -p "Recording... Press an option" <<-EOF
						Stop + Save
						Restart Recording
						Abort Recording
					EOF
					)"

					case "$STOP_OPTION" in
						"Stop + Save")
							kill $ARECORD_PID
							echo "Recording stopped. File '$SXMO_RECDIR/$NOW.wav'"
							break
							;;
						"Restart Recording")
							kill $ARECORD_PID
							rm "$SXMO_RECDIR/$NOW.wav"
							NOW="$(date '+%y%m%d_%H%M%S')4"
							sxmo_terminal.sh arecord -vv --format=cd "$SXMO_RECDIR/$NOW.wav" &
							ARECORD_PID=$!
							;;
						"Abort Recording")
							kill $ARECORD_PID
							rm "$SXMO_RECDIR/$NOW.wav"
							echo "Recording aborted. File '$SXMO_RECDIR/$NOW.wav' will be discarded."
							break
							;;
						*)
							# canceled or closed dmenu
							break
							;;
					esac
				done
				;;

			"Start opus recording")
				ffmpeg_check
				NOW="$(date '+%y%m%d_%H%M%S')"
				sxmo_terminal.sh ffmpeg -f alsa -i default -c:a libopus -b:a 128k "$SXMO_RECDIR/$NOW.opus" &

				while true; do
					STOP_OPTION="$(sxmo_dmenu.sh -p "Recording... Press an option" <<-EOF
						Stop + Save
						Restart Recording
						Abort Recording
						EOF
					)"

					case "$STOP_OPTION" in
						"Stop + Save")
							pkill ffmpeg
							echo "Recording stopped. File '$SXMO_RECDIR/$NOW.opus'"
							break
							;;
						"Restart Recording")
							pkill ffmpeg
							rm "$SXMO_RECDIR/$NOW.opus"
							NOW="$(date '+%y%m%d_%H%M%S')"
							sxmo_terminal.sh ffmpeg -f alsa -i default -c:a libopus -b:a 128k "$SXMO_RECDIR/$NOW.opus" &
							;;
						"Abort Recording")
							pkill ffmpeg
							rm "$SXMO_RECDIR/$NOW.opus"							
							echo "Recording aborted. File '$SXMO_RECDIR/$NOW.opus' will be discarded."
							break
							;;
						*)
							break
							;;
					esac
				done
				;;
 				"Audio Settings")
                sxmo_audio.sh
                ;;
                "Delete Recordings")
                delete_menu
                ;;
			*"Recordings")
				play_menu
				;;
			*)
				exit 0
		esac
	done
}

play_menu() {
	options="$(ls -1 "$SXMO_RECDIR")

- Abort -"
	selection=$(printf "%s" "$options" | sxmo_dmenu.sh -p "Select to play")

	if [ "$selection" = "- Abort -" ]; then
		recordmenu
	fi
	# Check if the selected file exists
	if [ -f "$SXMO_RECDIR/$selection" ]; then
		xdg-open "$SXMO_RECDIR/$selection"
	fi
}

delete_menu() {
	options="$(ls -1 "$SXMO_RECDIR")

- Abort -"
	selection=$(printf "%s" "$options" | sxmo_dmenu.sh -p "Select to delete")

	if [ "$selection" = "- Abort -" ]; then
		exit 0
	fi
	# Check if the selected file exists
	if [ -f "$SXMO_RECDIR/$selection" ]; then
		confirmation=$(printf "%s" "Yes

No" | sxmo_dmenu.sh -p "Are you sure?")

		if [ "$confirmation" = "Yes" ]; then
			rm -f "$SXMO_RECDIR/$selection"
			notify-send -t 1500 "Deleted" "'$selection'"
		fi
	fi
}

# if sxmo_records.sh is executed without argument, open menu
if [ -z "$1" ]; then
	set -- recordmenu
fi

"$@"
