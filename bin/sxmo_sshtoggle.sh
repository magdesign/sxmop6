#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-only
# Copyright 2022 Sxmo Contributors

# Note: this script should be run as root via doas

# shellcheck source=scripts/core/sxmo_common.sh
. sxmo_common.sh

on() {
     sxmo_terminal.sh -- sh -c 'rc-service sshd start && rc-update add sshd'
#     sxmo_notify_user.sh "SSH Enabled"
}

off() {
     sxmo_terminal.sh -- sh -c 'rc-service sshd stop && rc-update del sshd'
 #    sxmo_notify_user.sh "SSH Disabled"
}

case "$1" in
	on)
		on
		;;
	off)
		off
		;;
	*) #toggle
	if rc-service sshd status | grep -q "started"; then
	       off
		else
			on
		fi
esac
