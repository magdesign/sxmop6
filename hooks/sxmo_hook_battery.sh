#!/bin/sh
# configversion: d031535bc22ba19d69704317b8d50560
# Description: read values from UPower.conf and trigger actions
# Notes: v0.4 pocof1 version goes into: ~/.config/sxmo/hooks/

# read values from /etc/Upower/Upower.conf
CRITICAL=$(grep 'PercentageCritical=' /etc/UPower/UPower.conf | grep -v '^#' | cut -d'=' -f2)
ACTION=$(grep 'PercentageAction=' /etc/UPower/UPower.conf | grep -v '^#' | cut -d'=' -f2)

# read battery values
BATNAME=$(upower -e | grep 'battery')
PERCENTAGE=$(upower -i $BATNAME | awk '/percentage/; /^$/ {exit}' | sed 's/\([0-9]*\).[^0-9]*/\1/' | sed 's/%//g' | awk -F . '{print $1}')
STATE=$(upower -d | grep 'state' | grep -o '[^:]*$' | tr -d ' ' | sed -n '1p')
LED=/sys/class/leds/white:status/brightness

# to display the symbol in statusbar:
sxmo_hook_statusbar.sh battery "$BATNAME" "$STATE" "$PERCENTAGE"


if [ "$STATE" = "charging" ]; then 
	echo 255 > $LED 
	# make sure auto-suspend is disabled, if not, disable it
	if [ ! -f "$XDG_CACHE_HOME/sxmo/sxmo.nosuspend" ]; then
    	touch $XDG_CACHE_HOME/sxmo/sxmo.nosuspend
	fi
	sleep 5
else
	echo 0 > $LED
# todo: when auto suspend was enabled, re-enable it
fi

if [ "$STATE" = "discharging" ] && [ "$PERCENTAGE" -le "$ACTION" ]; then 
	notify-send --urgency=critical "no more energy!" "shutting down in 22s"
	sleep 22
	doas poweroff
	# sxmo_power.sh poweroff will not turn off the phone! 
elif [ "$STATE" = "discharging" ] && [ "$PERCENTAGE" == "$CRITICAL" ]; then 
	notify-send -t 60000 --urgency=critical "critical energy level" "only $PERCENTAGE % left"
	sleep 60 # to avoid a too fast message loop
	exit 0
fi
