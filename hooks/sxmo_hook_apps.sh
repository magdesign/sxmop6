#!/bin/sh
# configversion: c1e89f1954ce308dc3706f1c9859ba09
# SPDX-License-Identifier: AGPL-3.0-only
# Copyright 2022 Sxmo Contributors
# shellcheck disable=SC2154

# include common definitions \
# shellcheck source=configs/default_hooks/sxmo_hook_icons.sh
. sxmo_hook_icons.sh

write_line() {
	printf "%s ^ 0 ^ %s\n" "$1" "$2"
}

write_line_app() {
        executable="$1"
        label="$2"
        command="$3"
        if command -v "$executable" >/dev/null; then
                write_line "$label" "$command"
        elif ls /var/lib/flatpak/app/"$executable" >/dev/null; then
                write_line "$label" "$command"
#		elif ls ~/.local/share/containers/storage/volumes/"$executable" >/dev/null; then
#				write_line "$label" "$command"
        fi
}
# cam& gallery
write_line_app snapshot "$icon_cam snapshot" "snapshot"
write_line_app qcam "$icon_cam qcam" "qcam"
write_line_app org.kde.koko "$icon_img Gallery" "flatpak run org.kde.koko"
# web
write_line_app ncgopher "$icon_glb ncgopher" "sxmo_terminal.sh --font=monospace:size=8 ncgopher"
write_line_app firefox-esr "$icon_ffx Firefox ESR" "firefox-esr"
write_line_app bukuserver "$icon_tab BukuServer" "~/sxmop6/launchers/bukuserver_launcher.sh"
write_line_app firefox "$icon_ffx Firefox" "firefox"
write_line_app dillo "$icon_glb dillo" "dillo"
write_line_app org.chromium.Chromium "$icon_glb chromium" "flatpak run org.chromium.Chromium"
# translators
write_line_app podman "$icon_edt LibreTranslate" "sxmo_terminal.sh ~/LibreTranslate/run.sh --load-only en,de,es"
# messenger
write_line_app flare "$icon_msg Flare Signal" "flare"
write_line_app dino "$icon_msg Dino" "GTK_THEME=Adwaita:dark dino"
write_line_app nheko "$icon_msg Nheko" "nheko"
#write_line_app fractal "$icon_msg Fractal" "fractal"
write_line_app org.gnome.Fractal "$icon_msg Fractal" "flatpak run org.gnome.Fractal"
write_line_app curseddelta "$icon_msg cursedDelta" "sxmo_terminal.sh --font=monospace:size=7 curseddelta"
write_line_app chat.delta.desktop "$icon_msg Deltachat" "~/sxmop6/launchers/deltachat_launcher.sh"
write_line_app irssi "$icon_msg Irssi" "sxmo_terminal.sh irssi"
write_line_app hexchat "$icon_msg Hexchat" "hexchat"
write_line_app python "$icon_msg reticulum" "~/sxmop6/launchers/reticulum_launcher.sh"
# navigation
write_line_app osmin "$icon_map Osmin" "~/sxmop6/launchers/osmin_launcher.sh"
write_line_app pure-maps "$icon_map Pure-Maps" "~/sxmop6/launchers/puremaps_launcher.sh"
write_line_app osmscout-server "$icon_map osmscout-server" "~/sxmop6/launchers/osmscout_launcher.sh"
write_line_app mepo "$icon_map mepo" "mepo"
write_line_app page.codeberg.tpikonen.satellite "$icon_glb satellites" "flatpak run page.codeberg.tpikonen.satellite"
# various
write_line_app micro "$icon_gam llama" "~/sxmop6/launchers/llm_launcher.sh"
write_line_app podman "$icon_fnd yacy" "~/sxmop6/launchers/yacy_launcher.sh "
# multimedia
write_line_app amberol "$icon_mus Amberol" "amberol $XDG_MUSIC_DIR"
write_line_app io.github.seadve.Mousai "$icon_mus Mousai" "flatpak run io.github.seadve.Mousai"
write_line_app gpodder "$icon_rss gPodder" "gpodder"
write_line_app audacity "$icon_mic Audacity" "audacity"
write_line_app pavucontrol "$icon_mus Pavucontrol" "pavucontrol"

write_line_app gnome-calculator "$icon_clc Calculator" "gnome-calculator"
write_line_app calcurse "$icon_clk Calcurse" "sxmo_terminal.sh calcurse"
write_line_app chatty "$icon_msg Chatty" "chatty"
write_line_app cmus "$icon_mus Cmus" "sxmo_terminal.sh cmus"
write_line_app foliate "$icon_bok Foliate" "foliate"
write_line_app gedit "$icon_edt Gedit" "gedit"
write_line_app geeqie "$icon_img Geeqie" "geeqie"

write_line_app geopard "$icon_glb Geopard" "geopard"
write_line_app amfora "$icon_glb Amfora" "sxmo_terminal.sh --font=monospace:size=8 amfora"
write_line_app castor "$icon_glb Castor" "castor"

write_line_app gossip "$icon_glb Nostr Gossip" "gossip"
write_line_app com.zettlr.Zettlr "$icon_edt Zettlr" "flatpak run com.zettlr.Zettlr"
write_line_app gnome-weather "$icon_wtr Gnome Weather" "gnome-weather"
write_line_app gomuks "$icon_msg Gomuks" "sxmo_terminal.sh gomuks"
# tools & monitors
write_line_app htop "$icon_cfg Htop" "sxmo_terminal.sh htop"
write_line_app wiper "$icon_sav Wiper" "sxmo_terminal.sh --font=RobotoMono:size=8 wiper"
write_line_app zenith "$icon_cfg Zenith" "sxmo_terminal.sh --font=RobotoMono:size=8 zenith"

write_line_app ii "$icon_msg Ii" "sxmo_terminal.sh ii"
write_line_app kasts "$icon_rss Kasts" "kasts"
write_line_app kontact "$icon_msg Kontact" "kontact"
write_line_app konversation "$icon_msg Konversation" "konversation"
write_line_app koreader "$icon_bok KOReader" "koreader"
write_line_app kwrite "$icon_edt Kwrite" "kwrite"
write_line_app lagrange "$icon_glb Lagrange" "lagrange"
write_line_app lf "$icon_dir Lf" "sxmo_terminal.sh lf"
write_line_app luakit "$icon_glb Luakit" "luakit"
write_line_app marble "$icon_map Marble" "marble"
write_line_app midori "$icon_glb Midori" "midori"
write_line_app navit "$icon_gps Navit" "navit"
write_line_app ncmpcpp "$icon_mus Ncmpcpp" "sxmo_terminal.sh ncmpcpp"
write_line_app netsurf "$icon_glb Netsurf" "netsurf"
write_line_app newsboat "$icon_rss Newsboat" "sxmo_terminal.sh newsboat"
write_line_app com.gitlab.newsflash "$icon_rss Newsflash" com.gitlab.newsflash
write_line_app pulsemixer "$icon_mus Pulsemixer" "sxmo_terminal.sh pulsemixer"

write_line_app podboat "$icon_rss Podboat" "sxmo_terminal.sh podboat"
write_line_app dev.tchx84.Portfolio "$icon_dir Portfolio" "dev.tchx84.Portfolio"
write_line_app qutebrowser "$icon_glb Qutebrowser" "qutebrowser"
write_line_app ranger "$icon_dir Ranger" "sxmo_terminal.sh ranger"
write_line_app sacc "$icon_glb Sacc" "sxmo_terminal.sh sacc i-logout.cz/1/bongusta"
# tresors & wallets
write_line_app secrets "$icon_lck Secrets" "secrets"
write_line_app numberstation "$icon_clc Numberstation" "GTK_THEME=Adwaita:dark numberstation"
write_line_app org.getmonero.Monero "$icon_msg Monero" "flatpak run org.getmonero.Monero"

# graphics & video
write_line_app krita "$icon_img Krita" "krita --nosplash"
write_line_app org.kde.kdenlive "$icon_mvi Kdenlive" "flatpak run org.kde.kdenlive"
write_line_app inkscape "$icon_img Inkscape" "inkscape --nosplash"
write_line_app app.fotema.Fotema "$icon_img Fotema Gallery" "flatpak run app.fotema.Fotema"
write_line_app sic "$icon_msg Sic" "sxmo_terminal.sh sic"
([ "$SXMO_WM" = dwm ] && command -v st >/dev/null) && \
	write_line "$icon_trm St" "st -e $SHELL"
write_line_app surf "$icon_glb Surf" "surf"
write_line_app syncthing "$icon_rld Syncthing" "syncthing"
write_line_app syncthing-gtk "$icon_rld Syncthing GTK" "syncthing-gtk"
write_line_app thunar "$icon_dir Thunar" "sxmo_terminal.sh thunar"
write_line_app totem "$icon_mvi Totem" "totem"
write_line_app waydroid "$icon_and Waydroid" "waydroid show-full-ui"
write_line_app w3m "$icon_glb W3m" "sxmo_terminal.sh w3m duck.com"
write_line_app vlc "$icon_mvi Vlc" "vlc"
write_line_app vte-2.91 "$icon_trm VTE 3" "vte-2.91"
write_line_app vte-2.91-gtk4 "$icon_trm VTE 3 (GTK-4)" "vte-2.91-gtk4"
([ "$SXMO_WM" = dwm ] && command -v xcalc >/dev/null) && \
	write_line "$icon_clc Xcalc" "xcalc"
write_line_app xournalpp "$icon_bok Xournalpp" "xournalpp"
write_line_app zathura "$icon_bok Zathura" "zathura"4
write_line_app j4-dmenu-desktop "$icon_grd All apps" "j4-dmenu-desktop --dmenu=sxmo_dmenu.sh --term=sxmo_terminal.sh"
