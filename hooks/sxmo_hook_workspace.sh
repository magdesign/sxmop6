#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-only
# Copyright 2022 Sxmo Contributors

# Checks if a workspace is visible with conky
# if yes, we launch conky, if not, we kill it

# This should help saving a few % of cpu usage but
# we need to check how much cpu this hook itself takes.


while true; do
# Check if we have a workspace without anything launched
WORKSPACEWATCH=$(swaymsg -t get_tree | jq -r '.. | objects | select(.type? == "workspace" and (.nodes | length) == 0) | .name' | tail -n +2 )
STATUS=$(superctl status sxmo_conky | grep "Status:" | grep -o "started")

	if [ -z "$WORKSPACEWATCH" ]; then
		if [[ "$STATUS" == "started" ]]; then
			superctl stop sxmo_conky
		fi
	else
		if [[ "$STATUS" != "started" ]] && swaymsg -t get_outputs | grep -q '"power": true,'; then
		    superctl start sxmo_conky
		fi
	fi
# intervall
sleep 0.8
done
