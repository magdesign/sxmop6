#!/bin/sh
# configversion: 8c70b4331421862ea438b53fa6f991f8
# SPDX-License-Identifier: AGPL-3.0-only
# Copyright 2022 Sxmo Contributors

# For use with peanutbutter (peanutbutter --font Sxmo --statuscommand sxmo_hook_lockstatusbar.sh)
# This filters out the last component (which is usually the time and is already displayed more prominently

# make sure status bar icons are suited for peanutbutter
#sxmo_hook_statusbar.sh state_change

# obtain status output to pass to peanutbutter (this keeps running and updating to stdout)
#exec sxmo_status_watch.sh -o pango


# Obtain status output to pass to peanutbutter, using awk to remove the last
# column (the time), which we don't need duplicated. We also remove the · symbol which we use in $SXMO_NOTCH
# and is not needed for the lockscreen.

# Get the NOTCH value
NOTCH=$(grep "sxmobar -a notch" ~/.config/sxmo/hooks/sxmo_hook_statusbar.sh | awk -F'"' '{print $2}')

# escape special characters
esc_NOTCH=$(printf '%s' "$NOTCH" | sed 's/[\/&]/\\&/g; s/|/\\|/g; s/-/\\-/g')

# trigger the symols for lockscreen
sxmo_status_watch.sh -o pango  | tr -d "$esc_NOTCH" 

# what if there is no time (massive notch)? | awk 'NF{NF-=1};
