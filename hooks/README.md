## sxmo_hook_apps.sh
* custom app hook supporting flatpaks

## sxmo_hook_battery.sh
* OP6: warns when battery is low, 
* shows led when charger is connected
* powers off on critical

## sxmo_hook_battery_poco.sh
* Pocof1: warns when battery is low and
* powers off on critical value 
* shows led when charger is connected

## sxmo_hook_block_suspend.sh
* OP6: slider can block suspend
* block suspend when playing: mpv, amberol

## sxmo_hook_contextmenu.sh
* delete firefox cookies/history
* display-scaler in main menu
* screenshot in main menu
* copy sms menu
* filemanager = yazi
* removed: help, binaries, all apps

## sxmo_hook_inputhandler.sh
* vertical swipe for volume works only 
when any audio is playing
* topedge left swipe turns off flashlight
* removed buttom_left lockscreen gesture
* trigger photo on gnome snapshot with power button
 
## sxmo_hook_screenoff.sh
* stop conky when screen is off
https://wiki.postmarketos.org/wiki/User:Magdesign#Saving_Power

## sxmo_hook_statusbar.sh
* OP6: work around the notch

## sxmo_hook_statusbar_poco.sh
* Pocof1: remove clock and workaround the
massive notch

## sxmo_hook_workspace.sh
* checks if desktop is visible and enables conky
https://wiki.postmarketos.org/wiki/User:Magdesign#Saving_Power

## sxmo_hook_lockstatusbar.sh
* accept custom notch fillings with peanutbutter
