# Configs

## megapixels
see README.md inside folder

## mimeapps
see README.md inside folder

## ublock
rules for ublock origin the disable google login promt and block amazon and other annoying shit

## 00-sxmo.conf
enables mac randomisation
goes into: /etc/NetworkManager/conf.d/00-sxmo.conf

## 51-disable-suspensionn...
old workaround to fix audio, to be deleted soon...

## call_audio_idle_suspen....
old workaround to fix audio, to be deleted soon..

## config.toml
config for superfile (spf) on mobile screen

goes into ~/.config/superfile/

## conky.config
my personal conky file:

![screenshot](../userscripts/images/conky_sml.png "conky")

goes into ~/.config/sxmo/

## kdeglobals
dark theme for kde apps.

goes into ~/.config

`QT_QUICK_CONTROLS_STYLE=org.kde.breeze QT_QPA_PLATFORMTHEME=kde `

## krita_phone.kws
mobile layout for [krita](https://krita.org), see https://wiki.postmarketos.org/wiki/Sxmo/Tips_and_Tricks#Krita

goes into ~/.local/share/krita/workspaces/krita_phone.kws

![screenshot](../userscripts/images/krita_sml.png "krita_phone")

Krita Menu => Window => Workspaces => Import Workspace => select op6.2

## phn.kdenlivelayout

mobile layout for kdenlive, see https://wiki.postmarketos.org/wiki/Sxmo/Tips_and_Tricks#Kdenlive

goes into usr/share/mime/application/kdenlivelayout.xml

![screenshot](../userscripts/images/kdenlive_sml.png "kdenlive_phone")

Go to View => Manage Layouts and import the phn layout.

Make sure to delete all other layouts to get space for the menu.

Turn your phone layout to landscape (you might prior switch scaling to 2) and go to:

Settings => Configure Toolbars => TimelineToolbar

In the right window, remove:

    Track Menu
    Timeline Edit Menu
    Separator

Remove everything after the Timecode 00:00:00:00

After the Spacer Tool, add:

    Favorite Effects
    Delete Selected Item
    Insert Track...

![screenshot](images/conky_sml.png "conky")


## nerd-dictation.py
see https://wiki.postmarketos.org/wiki/User:Magdesign#Nerd_Dictation

## op6conky.conf
another conky conf

## policies.json
see https://wiki.postmarketos.org/wiki/User:Magdesign#Search_Engine

goes into /etc/firefox/policies/policies.json

## Reticulum
backup of relay node list

## sway
modified sway file to disble tray in swaybar, enable custom keybindings for keyboard and more, see wiki
goes into ~/.config/sxmo/sway

## sway_poco
same as above

## wofi_style.css
my custom menu style with red font

goes into ~/.config/wofi/style.css
 
