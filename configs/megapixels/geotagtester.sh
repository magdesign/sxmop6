#!/bin/sh

# check for geolocation
getlocation() {

# Ask for location and redirect output to a file
/usr/libexec/geoclue-2.0/demos/where-am-i > /tmp/location.log &
# Store PID
PID=$!

# Either 1 line to be written or a timeout of 35s
{
  # Check for lines written
  for i in $(seq 1 45); do
    sleep 1
    line_count=$(wc -l < /tmp/location.log)

    if [ "$line_count" -ge 1 ]; then
      kill $PID  # Kill the command lines are written
      exit 0
    fi
  done
  # Timed out
  notify-send "No location tag!"
  kill $PID  # Kill the command if still running
  exit 1
} &
wait $PID  # Wait for the background command to finish
}

# Start as background process and capture the pid
getlocation &
bg_pid=$!

# all image processing magic should go in here
imageprocessing(){
echo "image pricessing for 10s"
sleep 10
echo "finished imges"
}

# run image processing
imageprocessing

# Wait for getlocatiom process to complete
wait $bg_pid

# Check the exit status of getlocation
if [ $? -eq 0 ]; then
    echo "no location."
    echo "exit"
    exit 0
else
    echo "Location"
      lat_deg=$(grep -E 'Latitude' /tmp/location.log | awk '{print $2}' | head -n 1)
      lon_deg=$(grep -E 'Longitude' /tmp/location.log | awk '{print $2}' | head -n 1)
      alti=$(grep -E 'Altitude' /tmp/location.log | awk '{print $2}' | head -n 1)
      # Get N S E W directions
      # If latitude is positive, it's N; if negative, it's S.
      # If longitude is positive, it's E; if negative, it's W.
      if echo "$lat_deg" | grep -q 'S'; then
          lat_deg=$(echo "$lat_deg * -1" | bc)
          lat_dir="S"
      else
          lat_dir="N"
      fi

      if echo "$lon_deg" | grep -q 'W'; then
          lon_deg=$(echo "$lon_deg * -1" | bc)
          lon_dir="W"
      else
          lon_dir="E"
      fi
      echo "write tag"
# write coordinates to image
#exiftool -overwrite_original -GPSLatitude=$lat_deg -GPSLongitude=$lon_deg -GPSLatitudeRef=$lat_dir -GPSLongitudeRef=$lon_dir -GPSAltitude=$alti -GPSAltitudeRef=0 "$TARGET_NAME.jpg"
      echo "$lat_deg $lat_dir"
      echo "$lon_deg $lon_dir"
      echo "$alti" 
      echo "cleanup"
	  rm /tmp/location.log
	  echo "exit"
    exit 0
fi

exit 0
