# Megapixels 1.8.x

install gmic with:
`sudo apk add gmic`

copy the postprocessing scripts to:
`/usr/share/megapixels/postprocessor.d/`

the camera config goes to (the .ini is for v1.8.x the .conf for v2.x):
`/usr/share/megapixels/config/xiaomi,beryllium.ini`



before hitting the shutter,
make sure you selected `postprocess_auto.sh` in megapixels settings.

when location is on, it will geotag
your pictures.

