#!/bin/sh
# Author: magdesign
# Notes: v0.2 ols folder must be in $HOME

CMD="python3 -m ols -d debug -C /home/me/ols/ols/data/ols-example-conf.toml"
wwan_status=$(nmcli radio wwan)

launch_ols(){
	cd ~/ols
	source ols_venv/bin/activate
	$CMD
}

if pgrep -f "$CMD" > /dev/null; then
    echo "stopping OLS..."
	pkill -f "$CMD"
	# only turn off gsm when previously was off
	if [ -e "$XDG_CACHE_HOME/sxmo/sxmo.gsmwasoff" ]; then
		nmcli radio wwan off
		notify-send -u critical -t 1000 "disable ols" "disable gsm"
		rm "$XDG_CACHE_HOME/sxmo/sxmo.gsmwasoff"
 	fi
	exit 0
else
	# check if gsm was already on
if [[ "$wwan_status" == *"disabled"* ]]; then
	touch "$XDG_CACHE_HOME/sxmo/sxmo.gsmwasoff"
	notify-send -t 1000 "enable ols" "enable gsm"
fi 
	nmcli radio wwan on
    echo "starting OLS..."
	launch_ols
fi

exit 0
