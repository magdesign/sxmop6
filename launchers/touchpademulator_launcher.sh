#!/bin/sh
# title="icon_usr Touchpad"
# Author: magdesign
# Description: toggles touchpad emulator
# Notes: v0.3 requires: TouchpadEmulator

# check requirements
if command -v TouchpadEmulator >/dev/null 2>&1 ;then
echo "is installed"
else
  notify-send --urgency=critical "Please install patched TouchpadEmulator"
  exit 1
fi

# Check if 'TouchpadEmulator' is running
if pgrep -f TouchpadEmulator > /dev/null
then
	pkill -f TouchpadEmulator
	sxmobar -d touch 
else
    # is not running, start it
	if [[ $( swaymsg -t get_outputs | grep transform | awk '{print $2}') == '"normal",' ]]; then
	sxmobar -f green -a touch 68 󰍽 
	/usr/bin/TouchpadEmulator --no-buttons --no-slider --rotation-override 0
	exit 0
	else
	sxmobar -f green -a touch 68 󰍽 
	/usr/bin/TouchpadEmulator --no-buttons --no-slider --rotation-override 90
	exit 0
	fi
fi
exit 0
