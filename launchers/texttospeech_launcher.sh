#!/bin/sh
# title="icon_spk TextToSpeech"
# Author: magdesign
# Description: text-to-speech, replaces all previous tts scripts
# Notes: v0.4 requires: piper-tts py3-piper-tts tesseract ZXingReader wl-paste

# todo: auto language selection/detection based on content with
# https://tika.apache.org/
# java -jar tika-app-1.18.jar -l filetodetect > detectedlangfile 2>/dev/null

check_requirements(){
if command -v piper &> /dev/null
then
    echo "piper-tts is installed"
else
  notify-send --urgency=critical "install piper-tts and a model first"
  exit 1
fi
}

check_ocr_requirements(){
	if ! command -v tesseract >/dev/null 2>&1 ;then
		notify-send --urgency=critical "Please install tesseract-ocr tesseract-ocr-data-eng"
  		exit 1
	fi

	if ! command -v wl-paste >/dev/null 2>&1 ;then
  		notify-send --urgency=critical "Please install wl-clipboard"
  		exit 1
	fi
	
	if ! command -v ZXingReader >/dev/null 2>&1 ;then
		notify-send --urgency=critical "Please install zxing"
  		exit 1
	fi
}

run_ocr(){
	# Check if already running
	if pgrep -f piper> /dev/null; then
    	echo "stop it"
		pkill -f piper
	else
		check_ocr_requirements
		# temp dir/files
		IMAGE=/tmp/ocr_tmp.png
		TEXT=/tmp/ocr_tmp

		# create a screenshot of wanted area:
		area="$(slurp -d)"
		grim -g "$area" $IMAGE

		# use barcode reader
		ZXingReader "$IMAGE" | awk '/^Text:/ {print $2}'| awk '{gsub(/^"|"$/,"")}1' > $TEXT.txt
		# check if we found something
		if [[ -s $TEXT.txt ]]; then
  			# displays extracted qr content
  			# notify-send "$(cat $TEXT.txt)"
  			# and copy text to clipboard
  			cat $TEXT | wl-copy
		else
  			rm $TEXT.txt
  			# sends image to ocr and extracts text 
  			tesseract -l eng+rus $IMAGE $TEXT
  			# displays text
  			# notify-send "$(cat $TEXT.txt | tr -d '\n')"
  			# notify-send "$(cat $TEXT.txt)"
  			# copy to clipboard
  			cat $TEXT.txt | wl-copy
  			# copy to clipboard without newlines
		fi

		# cleanup
		rm $IMAGE
		rm $TEXT.txt
	fi
}


list_languages(){
    options="$(ls -d $HOME/piper/models/* | xargs -n 1 basename)
\n- Abort -"
    
    text=$(echo -e "$options" | /usr/bin/sxmo_dmenu.sh -p "Enter text")
#    echo "$text" > /tmp/text

    # Handle the user's selection
    case "$text" in
        "de")
			speak_deu
            exit 0
            ;;
        "en")
			speak_eng
            exit 0
            ;;
        "es")
			speak_spa
            exit 0
            ;;
        "fi")
			speak_fin
            exit 0
            ;;
        "fr")
			speak_fra
            exit 0
            ;;
        "- Abort -")
            exit 0
            ;;
        *)
          #  echo "No valid option selected."
            ;;
    esac
}

speak_spa(){
	DIR="$HOME/piper/models/es/"
	FILE1=es_MX-claude-high.onnx 
	FILE2=es_es_MX_claude_high_es_MX-claude-high.onnx.json

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2" ]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi

	# speak
	wl-paste | tr -d '\n' | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay --buffer-size=1024 -r 22050 -f S16_LE -t raw -
}


speak_eng(){
	DIR="$HOME/piper/models/en/"
	FILE1="en_GB-alan-medium.onnx"
	FILE2="en_en_GB_alan_medium_en_GB-alan-medium.onnx.json"

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2" ]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi
	# speak
	wl-paste | tr -d '\n' | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay --buffer-size=1024 -r 22050 -f S16_LE -t raw -
}


speak_fin(){
	DIR="$HOME/piper/models/fi/"
	FILE1="fi_FI-harri-medium.onnx"
	FILE2="fi_fi_FI_harri_medium_fi_FI-harri-medium.onnx.json"

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2" ]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi

	# speak
	wl-paste | tr -d '\n' | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay --buffer-size=1024 -r 22050 -f S16_LE -t raw -
}

speak_deu(){
	DIR="$HOME/piper/models/de/"
	FILE1="de_DE-thorsten-low.onnx"
	FILE2="de_de_DE_thorsten_low_de_DE-thorsten-low.onnx.json"

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2" ]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi

	# speak without new lines
	wl-paste | tr -d '\n' | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay --buffer-size=8192 -r 16000 -f S16_LE -t raw -
}

speak_fra(){
	DIR="$HOME/piper/models/fr/"
	FILE1="fr_FR-upmc-medium.onnx"
	FILE2="fr_fr_FR_upmc_medium_fr_FR-upmc-medium.onnx.json"

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2" ]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi
	# speak
	wl-paste | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay --buffer-size=1024 -r 22050 -f S16_LE -t raw -
}

# run programm
check_requirements

if [ "$1" = "ocr" ]; then
	run_ocr
	list_languages
elif [ "$1" = "en" ]; then
	speak_eng
elif [ "$1" = "de" ]; then
	speak_deu
elif [ "$1" = "fi" ]; then
	speak_fin
elif [ "$1" = "es" ]; then
	speak_spa
elif [ "$1" = "fr" ]; then
	speak_fra	
else
    echo "Launch with ocr, en, de, fi, es, fr e.g. 'texttospeech_launcher.sh ocr'"
fi

exit 0
