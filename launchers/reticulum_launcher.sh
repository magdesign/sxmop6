#!/bin/sh
# title="icon_red reticulum"
# Author: magdesign
# Description: toggle reticulum
# Notes: v0.2
# check if reticulum is there

IDENTITY="$HOME/reticulum-meshchat/storage/identity"
STORAGE="$HOME/reticulum-meshchat/storage/"

if [ -f "$HOME/reticulum-meshchat/meshchat.py" ]; then
   echo "reticulum-meshchat is installed "
else
   echo "reticulum-meshchat does not exist ~"
	exit 1
fi
# check if identity is there
if [ -f "$IDENTITY" ]; then
	echo "identity file is there"
else
	echo "create an identity first in $IDENTITY"
	exit 0
fi

# toggle start / stop
if ! pgrep -f "meshchat.py" > /dev/null
	then
    echo "starting..."
	notify-send -t 1500 "starting reticulum"
	python ~/reticulum-meshchat/meshchat.py --identity-file $IDENTITY --storage-dir $STORAGE &
#	sxmobar -f white -a reticulum 6 ""	
else
	pkill -f meshchat.py
    echo "stopped"
#	sxmobar -d reticulum
	notify-send -u "critical" -t 5000 "reticulm stopped"
fi

exit 0
