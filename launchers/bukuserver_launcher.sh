#!/bin/sh
# title="icon_tab Bukuserver"
# Author: magdesign
# Description: launches the bukuserver
# Notes: v0.1

# Check if bukuserver is installed
if ! command -v bukuserver >/dev/null 2>&1; then
    notify-send "bukuserver is not installed"
    exit 1
fi

# Check if bukuserver is running
if pgrep -x "bukuserver" >/dev/null 2>&1; then
	pkill bukuserver
else
	# start it
	bukuserver run --host 127.0.0.1 --port 5001 &
	xdg-open "http://127.0.0.1:5001"
fi

exit 0
