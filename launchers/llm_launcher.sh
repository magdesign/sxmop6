#!/bin/sh
# title="icon_red llm-chatbot"
# Author: magdesign
# Description: toggle llama-server
# Notes: v0.4 requires compiled  llama.cpp in $HOME

# check if llama is there
requirement_checks(){
	if [ -d "$HOME/llama.cpp" ]; then
   		echo "llama.cpp is there"
	else

   		echo "llama.cpp does not exist"
   	exit 1
	fi
}

power_check(){
	battery_percent=$(upower -d | grep percentage | awk -F':' '{print $2}' | sed 's/%//g' | head -n 1)

	if [ "$battery_percent" -lt 12 ]; then
    	notify-send -u critical  "Battery level too low" "its only $battery_percent%"
    exit 1
	fi
}


list_models=$(ls ~/llama.cpp/models/ | grep -v '^ggml')

menu() {
if ! pgrep llama > /dev/null
then
	MENUINPUT="$(sxmo_dmenu.sh -p "Select Model" <<EOF
$list_models

Abort
EOF
	)" || exit
	case "$MENUINPUT" in
		"Abort")
			exit 0
			;;
		*)
			# wrong/no input
			echo "no selection"
			;;
	esac

power_check

#start llama with selected model
	~/llama.cpp/./llama-server -m ~/llama.cpp/models/$MENUINPUT --port 8080 &
	xdg-open http://localhost:8080 &
	exit 0
else
	pkill llama
    notify-send -u critical "llama stopped"
fi
exit 0
}

requirement_checks
menu

exit 0
