#!/bin/sh
# title="icon_mic Dictation"
# Author: magdesign
# Description: toggles speech to text input
# Notes: v0.4 requires: nerd-dictation

# check requirements
if [ -f ~/nerd-dictation/nerd-dictation ];  >/dev/null 2>&1 ;then
echo "seems to be installed"
else
  notify-send --urgency=critical "Please install nerd-dictation or adjust path"
  exit 1
fi

if ls ~/.config/nerd-dictation/model &> /dev/null; then
	echo "vosk model found"
else
  notify-send --urgency=critical "There are no configured models in" "~/.config/nerd-dictation/model/"
  exit 1
fi

if [ -f ~/.config/nerd-dictation/nerd-dictation.py ]; > /dev/null 2>&1; then
echo "nerd-dictation.py found"
else
  notify-send --urgency=critical "There is no nerd-dictation.py in" "~/.config/nerd-dictation/model/"
  exit 1
fi

# Check if it is running
if pgrep -f nerd-dictation> /dev/null
then
    echo "stop it"
	#or just add nerd-dictation end ?
	pkill -f nerd-dictation
	sxmobar -d dicta
else
    echo "start dictation"
    # is not running, start it 
    # search your cirrect device with: arecord -l
    export AUDIODRIVER=alsa
    AUDIODEV='hw:0,1'
	~/nerd-dictation/./nerd-dictation begin --simulate-input-tool=DOTOOL --input=SOX --config=$HOME/.config/nerd-dictation/nerd-dictation.py --vosk-model-dir=$HOME/.config/nerd-dictation/model/ &
	sxmobar -f green -a dicta 59 󰍬 

fi
