#!/bin/sh
# title="icon_fnd Yacy Search"
# Author: magdesign
# Description: launches the yacy container
# Notes: v0.1 requires: podman

# check if container exists
if podman inspect yacy > /dev/null 2>&1; then
    echo "yacy container found"
else
    echo "no yacy container installed"
	echo "will be downloaded now..."
	podman run -d --name yacy_search_server -p 8090:8090 -p 8443:8443 -e YACY_NETWORK_UNIT_AGENT=Choomba -v yacy_search_server_data:/opt/yacy_search_server/DATA --restart unless-stopped --log-opt max-size=200m --log-opt max-file=2 yacy/yacy_search_server:aarch64-latest
fi

# toggle to start/stop
if podman ps | grep docker.io/yacy/yacy_search > /dev/null 2>&1; then
		echo "already running, stop it"
		podman stop $(podman ps | grep yacy | awk '{print $1}')
		notify-send "yacy stopped"
		# firewall close port
		#sudo iptables -A INPUT -p tcp --dport 8090 -j DROP
		exit 0
	else
		echo "not running, start it"
		#firewall open port
		#sudo iptables -A INPUT -p tcp --dport 8090 -j ACCEPT
		podman run -p 8090:8090 -p 8443:8443 -e YACY_NETWORK_UNIT_AGENT=Choomba -v yacy_search_server_data:/opt/yacy_search_server/DATA --restart unless-stopped --log-opt max-size=200m --log-opt max-file=2 yacy/yacy_search_server:aarch64-latest &
#		echo "wait a moment to innitialize..."
		sleep 5
		notify-send "yacy started" "localhost:8090"
#		echo "running on :8090"
		xdg-open http://localhost:8090
fi

# backup data: 
#podman stop yacy_search_server
#podman run --rm -v yacy_search_server_data:/opt/yacy_search_server/DATA -v /tmp:/tmp openjdk:8-stretch bash -c "cd /opt/yacy_search_server && tar -cf - DATA | xz -q -3v -T0 > /tmp/DATA.tar.xz"
#podman start yacy_search_server

exit 0
