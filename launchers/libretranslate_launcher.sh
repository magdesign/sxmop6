#!/bin/sh
# title="icon_spk libretranslateServer"
# Author: magdesign
# Description: toggle libretranslate-server
# Notes: v0.3

# check if Libretranslate is in home folder
if ! [ -d "$HOME/LibreTranslate" ]; then
	    notify-send "install Libretranslate" "in $HOME"	
	exit 1
fi

# Query for  all containers
query_docker=$(podman ps -a --format "{{.ID}} {{.Image}}")
container_id=$(echo "$query_docker" | grep "libretranslate*" | awk '{print $1}')

# Check if a LibreTranslate container is running
if printf "$query_docker" | grep -q "libretranslate"; then
	notify-send -u critical -t 10000 "LibreTranslate" "stopping..."
	podman stop "$container_id"
else
	"$HOME/LibreTranslate/run.sh" > /dev/null 2>&1 &
	sleep 3
	if podman ps | grep -q "libretranslate*"; then
		notify-send "LibreTranslate" "running..."
	fi
	sleep 2
	xdg-open http://localhost:5000 &
fi

exit 0
