#!/bin/sh
# title="$icon_map OsmscoutServer"
# Author:magdesign
# Description: toggles osmscout-server
# Notes: v0.1 requires: pure-maps, osmscout-server

# check requirements
if command -v osmscout-server >/dev/null 2>&1 ;then
echo "osmscout is installed"
else
  notify-send --urgency=critical "Please install osmscout server"
  exit 1
fi

if pgrep  osmscout-server > /dev/null
	then
	pkill osmscout-server
	notify-send "osmscout-server" "stopped!"
	sxmobar -d osmscout
exit 0
	else
	osmscout-server  >/dev/null 2>&1 &
	sxmobar -f green -a osmscout 6 "󰍍"
	notify-send "osmscout-server" "started"
fi

exit 0
