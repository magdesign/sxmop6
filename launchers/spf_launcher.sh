#!/bin/sh
# title="icon_dir Superfile"
# Author: magdesign
# Description: launches spf
# Notes: v0.3 requires: sway, superfile
# replace command sxmo_hook_contextmenu.sh 
# under: 		# Default system menu (no matches)
# $icon_dir Files  ^ 0 ^ /path/to/this/script.sh

# check if spf exists
if command -v spf > /dev/null 2>&1; then
    echo "superfile found"
else
    echo "no spf installed"
fi

# get sway scaling value
SCALE=$(swaymsg -t get_outputs | grep scale | awk '{print $2}' | sed 's/[^0-9.]//g')
echo $SCALE

if [[ $(echo "$SCALE <= 2" | bc) -eq 1 ]]; then
    echo 'low enough'
	sxmo_terminal.sh /usr/local/bin/spf
else
   # echo 'scaling down to 1.5'
	#swaymsg "output \"DSI-1\" scale 1.5"
	sxmo_terminal.sh --font=monospace:size=7 /usr/local/bin/spf
fi

# reset scaling
# swaymsg "output \"DSI-1\" scale $SCALE"

exit 0
