#!/bin/sh
# title="$icon_gps PureMaps"
# Author:magdesign
# Description: enables geoclue agent and starts puremaps
# Notes: v0.7.1 requires: pure-maps, geoclue, gnss

# check requirements
if command -v pure-maps >/dev/null 2>&1 ;then
echo "pure-maps is installed"
else
  notify-send --urgency=critical "Please install Pure Maps"
  exit 1
fi
# should we check for gnss too?
VERSION=$(mmcli --version | awk 'NR==1 {print $2}')
echo "we have modemanager $VERSION"
# mmcli version check
if [[ "$VERSION" > "1.23.4" ]]; then
    echo "modern enough to use gnss which is"
	if [[ $(rc-service gnss-share status | awk '{print $3}') == "started" ]]; then
 		echo "running"
		/usr/libexec/geoclue-2.0/demos/agent  > /dev/null 2>&1 &
 	else
 		echo "starting"
		# enable gnss
		sxmo_terminal.sh -- sh -c 'doas rc-service gnss-share start '
		/usr/libexec/geoclue-2.0/demos/agent  > /dev/null 2>&1 &
		notify-send "gnss started" "geoclue running"
	fi
else
	notify-send -u critical "ModemManager $VERSION" "Upgrade"
	# warn if modem disabled version lower 1.23.4 
	if  [ $(nmcli radio wwan) == "disabled" ]; then
		notify-send -u critical "Enable Modem first!" "exited"
		exit 1
	else
		echo "modem is enabled, now geoclue"
		# enable gps (get is optional)
		mmcli -m any --location-enable-gps-raw
		mmcli -m any --location-enable-gps-nmea
		#mmcli -m any --location-get
		sleep 0.5
		# launch geoclue
		/usr/libexec/geoclue-2.0/demos/agent  > /dev/null 2>&1 &
		sleep 0.5
		notify-send "geoclue running"
	fi
fi

# check if we are online, if not, check if osmscout-server is running
ping -c 1 postmarketos.org >/dev/null 2>&1

if [ $? -ne 0 ]; then
    echo "we are offline, launch server"
	if pgrep  osmscout-server > /dev/null
	then
    echo "osmscout-server already running"
	else
	notify-send "offline!" "launching server"
	osmscout-server &
	sxmobar -f green -a osmscout 6 "󰍍"
	fi
fi

# launch
pure-maps

exit 0
