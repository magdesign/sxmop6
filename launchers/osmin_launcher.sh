#!/bin/sh
# title="$icon_gps Osmin"
# Author: magdesign
# Description: this script enables geoclue agent and starts osmin
# Notes: v0.5.6 Requires: osmin, geoclue, gnss-share

# check requirements
if command -v osmin >/dev/null 2>&1 ;then
echo "osmin installed"
else
  notify-send -u critical "Please install osmin"
  exit 1
fi
# should we check for gnss too?
VERSION=$(mmcli --version | awk 'NR==1 {print $2}')
echo "we have $VERSION"

# check mmcli version
if [[ "$VERSION" > "1.23.4" ]]; then
		echo "modern enough to use gnss which is"
		if [[ $(rc-service gnss-share status | awk '{print $3}') == "started" ]]; then
 		echo "running"
 		echo "enbable geoclue"
    	/usr/libexec/geoclue-2.0/demos/agent  > /dev/null 2>&1 &
 		else
 		echo "to be started"
		# enable gnss
#		sxmo_terminal.sh -- sh -c "doas rc-service gnss-share start"
		doas rc-service gnss-share start &
		notify-send "gnss started"
		echo "enable geocl"
    	/usr/libexec/geoclue-2.0/demos/agent  > /dev/null 2>&1 &
		fi
else
	 notify-send -u critical "ModemManager $VERSION" "Upgrade!"
# warn if modem disabled version lower 1.23.4 
	if  [ $(nmcli radio wwan) == "disabled" ]; then
		notify-send -u critical "Enable Modem first!" "exited"
		exit 1
	else
		echo "modem is enabled, now geoclue"
		# enable gps (get is optional)
		mmcli -m any --location-enable-gps-raw
		mmcli -m any --location-enable-gps-nmea
		#mmcli -m any --location-get
		sleep 0.5
	echo "launch geoclue"
	'/usr/libexec/geoclue-2.0/demos/agent  > /dev/null 2>&1' &
	sleep 0.5
	notify-send "gnss running" "geoclue running"
	fi
fi

#launch osmin
osmin 

exit 0
