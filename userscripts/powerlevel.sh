#!/bin/sh
# title="$icon_bat_c_3 Powerlevel"
# Author: magdesign
# Description:Shows battery level and state
# Note: v0.3

# read percentage
PERCENTAGE=$(upower -d | grep percentage | awk -F':' '{print $2}' |  sed 's/%//g' | head -n 1)
#PERCENTAGE=$(upower -d | awk '/percentage/; /^$/ {exit}' | sed 's/[^0-9]//g')
STATE=$(upower -d | awk '/state/; /^$/ {exit}' |  sed 's/state://g;s/ //g')
EMPTY=$(upower -d | awk '/ time to empty/; /^$/ {exit}' | sed 's/ time to empty://g;s/ //g')
FULL=$(upower -d | awk '/ time to full/; /^$/ {exit}' | sed 's/ time to full://g;s/ //g')
CURRENT1=$(cat /sys/class/power_supply/pmi8998-charger/hwmon2*/curr1_input)

#check for n times
for i in $(seq 1 5); do

if [ "$STATE" == "charging" ]; then
	if [ "$CURRENT1" -lt 400 ]; then
        notify-send -u critical -t 1980 "$PERCENTAGE%"" Battery ""$STATE -> $CURRENT1 mAh" "$FULL left"
	elif [ "$CURRENT1" -gt 1400 ]; then
        notify-send -u normal -t 1980 "$PERCENTAGE%"" Battery ""$STATE -> $CURRENT1 mAh" "$FULL left"
	else
        notify-send  -t 1980 "$PERCENTAGE%"" Battery ""$STATE -> $CURRENT1 mAh" "$FULL left"
	fi

elif [[ "$PERCENTAGE" -lt 15 ]]; then
	notify-send -t 1980 -u critical "$PERCENTAGE""%"" Battery LOW and ""$STATE" "$EMPTY"" left"
else        
	notify-send -t 1980 "$PERCENTAGE""%"" Battery ""$STATE" "$EMPTY"" left"

fi

sleep 2
done


