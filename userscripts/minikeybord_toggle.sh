#!/bin/bash
# title="$icon_tof miniKeyboard"
# Description: Toggles mini keyboard
# Author: magdesign
# Note: v0.1
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

path_of_script="$(cd "$(dirname "$0")"; pwd)"
#echo $path_of_script

if pgrep  wvkbd-mini > /dev/null
	then
	pkill wvkbd-mini > /dev/null
    sed -i 's/^# title="\$icon_ton miniKeyboard"/# title="\$icon_tof miniKeyboard"/' $path_of_script'/minikeybord_toggle.sh'
	else
	/usr/local/bin/wvkbd-mini &
    sed -i 's/^# title="\$icon_tof miniKeyboard"/# title="\$icon_ton miniKeyboard"/' $path_of_script'/minikeybord_toggle.sh'
fi

