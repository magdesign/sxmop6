#!/bin/sh
# title="$icon_gps GPS enable"
# Description: enables gps
# Author: magdesign
# License: MIT
# Notes: v0.4alpha added gnss
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

#should we check for gnss-share ?

# check if gps/geoclue is enabled, then toggle
# but what hapoens if one is on, the other not?

# check if geoclue is already running
if ! pgrep -f /usr/libexec/geoclue-2.0/demos/agent >/dev/null
then
echo not running
# start now ?
else
echo already running
fi

#to do: add toggler

VERSION=$(mmcli --version | awk 'NR==1 {print $2}')
echo we have $VERSION
# check mmcli version
if [[ $(echo "$VERSION '1.23.4'" | awk '{print ($1 > $2)}') == 1 ]]; then
    echo "modern enough to use gnss which is"
		if [[ $(rc-service gnss-share status | awk '{print $3}') == "started" ]]; then
 		echo running
 		else
 		echo starting
# ask for sudo and execute
		sxmo_terminal.sh -- sh -c 'doas rc-service gnss-share start '
# starting geoclue
	/usr/libexec/geoclue-2.0/demos/agent  > /dev/null 2>&1 &
	notify-send "gps & geoclue running"
		fi
else
# warn if modem disabled version lower 1.23.4 
	if  [ $(nmcli radio wwan) == "disabled" ]; then
	 notify-send -u critical "Enable Modem first!" "exited"
	exit 1
	else
		echo "modem is enabled, now geoclue"
		# enable gps (get is optional)
		mmcli -m any --location-enable-gps-raw
		mmcli -m any --location-enable-gps-nmea
		#mmcli -m any --location-get
		sleep 0.5
	# launch geoclue
	/usr/libexec/geoclue-2.0/demos/agent  > /dev/null 2>&1 &
	sleep 0.5
	notify-send "gps & geoclue running"
	fi
fi

exit 0
