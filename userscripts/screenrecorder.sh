#!/bin/sh
# title="$icon_cam Screenrecorder"
# Author: magdesign
# Description: works only on devices where vol button is input/event3
# Requirements: ffmpeg wf-recorder
# Notes:v0.2  Works only on sway/wayland + op6
# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# to do: modify stop with volume button for all sxmo devices 

# Check if wf-recorder ffmpeg is installed
if command -v wf-recorder >/dev/null 2>&1 ;then
echo "recorder installed"
else
  notify-send --urgency=critical "Please install wf-recorder"
  exit 1
fi

if command -v ffmpeg >/dev/null 2>&1 ;then
echo "ffmpeg installed"
else
  notify-send --urgency=critical "Please install ffmpeg"
  exit 1
fi

# save resulting movie to (i think mp4 can not handle free resolution formats):
file=$(date +%Y-%m-%d-%H-%M-%S)".mp4"
folder=~/Videos/

# instructions
notify-send -t 2000  "make a selection" "very small one makes fullscreen"


	area="$(slurp -o)"
	
	if [ -z "$area" ]; then
		area="$(swaymsg -t get_outputs | jq -r '.[] |select (.focused) | .rect | "\(.x),\(.y) \(.width)x\(.height)"')"
	fi
	wxh=$(echo "$area" | cut -d " " -f 2)
	w=$(echo "$wxh" | cut -d "x" -f 1)
	h=$(echo "$wxh" | cut -d "x" -f 2)
	if [ -n "$wxh" ] && [ "$w" -gt 9 ] && [ "$h" -gt 9 ]; then

# workaround the shitty double quote input bug :)
echo wf-recorder -g "\"$area\"" -f $folder$file > /tmp/rec
fi
# display instructions
notify-send -t 2000 --urgency=critical "after 5s it stops" "code a stop function"
sleep 2

# countdown until start:
precount=2

countdown() {
  seconds=$1
  while [ $seconds -gt 0 ]; do
    notify-send -t 800 "$seconds seconds until.." "$duration seconds recording"
    sleep 1
    seconds=$((seconds - 1))
  done
}
# execute countdown
countdown $precount

# here we finally record and move on
command=$(cat /tmp/rec)
#eval $command &
$command &
echo "recorder started here..."

# debug section, since kill/stop does not work:
echo "go to sleep"
sleep 5
echo "sleep is over"

	kill $(pgrep wf-recorder)
echo "killed ?"
# stop when volume is pressed
# must be added in sxmo_hook_contextmenu.sh
# then you can remove above lines

# end message
notify-send --urgency=critical "done...see:" "$file"

# cleanup 
rm -rf /tmp/rec
clear

echo finito
exit
