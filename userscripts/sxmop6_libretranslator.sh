#!/bin/sh
# title="$icon_usr LibreTranslate"
# Description: Translates text in wofi/bmenu
# Author: magdesign
# Notes: v0.3.1
# needs libretranslate docker installed and running
# translations are automatically saved in clipboard to paste in any application

# to do: fix growth of menu selectors on long input, to be fixed in show_translation or wofi?
# todo2: add ocr to translate

requirements_check(){
if ! [ -d "$HOME/LibreTranslate" ]; then
	    notify-send "install Libretranslate" "in $HOME"	
	exit 1
fi
	if ! command -v curl >/dev/null 2>&1; then
	    notify-send "please install curl"
    exit 1
fi
}

speak_check(){
# check for speak-engine
if ! command -v piper >/dev/null 2>&1; then
    notify-send "installed piper-tts & py3-piper-tts"    
	exit 1
fi
}

# check if libretranslate container is running. use podman-docker.
check_docker() {
query_docker=$(docker ps -a --format "{{.Image}}  {{.Ports}}")
	if echo "$query_docker" | grep "libretranslate" ; then
		echo "running"
	else
		"$HOME/LibreTranslate/run.sh" > /dev/null 2>&1 &
		notify-send -t 1000 -w "Libretranslate" "up in 5" 
		notify-send -u low -t 1000 -w "Libretranslate" "up in 4" 
		notify-send -t 1000 -w "Libretranslate" "up in 3" 
		notify-send -u low -t 1000 -w "Libretranslate" "up in 2" 
		notify-send -t 1000 -w "Libretranslate" "up in 1" 
#        sleep 5 # uncomment on slow systems!
	fi
	url="127.0.0.0:$(podman ps | grep 'libretranslate' | grep -oP '\d+(?=/tcp)' | head -n 1)"

}

ask_languages() {
	#url="127.0.0.0:5000"
	# localhost will not work!
	url="127.0.0.0:$(podman ps | grep 'libretranslate' | grep -oP '\d+(?=/tcp)' | head -n 1)"
    language_url="http://${url}/languages"
	#echo "$url"
    response=$(curl -s "$language_url")
    if [ -n "$response" ]; then
        response_parsed=$(printf "%s" "$response" | jq -r '.[] | "\(.code): \(.name)"' | sed 's/:.*//g')
    else
        echo "No languages found"
    fi
}

list_languages(){
			# create language pairs of all languages 
			# Convert space-separated string into an array
			set -- $response_parsed
			# Generate all possible pairs and store them in a variable
			# Create a temporary file
			temp_file=$(mktemp)
			# Generate all possible pairs and write to the temporary file
			for lang1 in "$@"; do
    			for lang2 in "$@"; do
        			if [ "$lang1" != "$lang2" ]; then
            			echo "$lang1 $lang2" >> "$temp_file"
        			fi
    			done
			done

			# For debugging
			# echo "Output written to: $temp_file"
			# Read options from the file and add "- Abort -"
			options=$(cat "$temp_file")
			options="$options

- Abort -"
			# create the menu
			lang=$(printf "$options" | /usr/bin/sxmo_dmenu.sh -p "Select Language")    

    		# Handle user's selection
    		case "$lang" in
        		"- Abort -")
            		exit 0
            		;;
        		*)
          		#  echo "No valid option selected."
            		;;
    		esac
}


gettext() {
    options="- Abort -"
    
    text=$(printf "%s" "$options" | /usr/bin/sxmo_dmenu.sh -p "Enter text")
    echo "$text" > /tmp/text
    case "$text" in
        "- Abort -")
            exit 0
            ;;
        *)
          #  echo "No valid option selected."
            ;;
    esac
}


translate() {
    		if [ -n "$text" ]; then
        		# notify-send "Translating '$text' to '$lang'"
        		path="/tmp/text"
			else
				notify-send "no text entered"
				gettext
			fi

			translate_url="http://${url}/translate"
			echo "translate URL: $translate_url"
			headers="Content-Type: application/json"
			# Split the string into two parts using the hyphen as a delimiter.
			in_lang=$(echo "$lang" | cut -d' ' -f1 | cut -d'=' -f2 | tr -d '"')
			out_lang=$(echo "$lang" | cut -d' ' -f2 | tr -d '"')
			# result without alternatives:
			#result=$(curl -sAX POST -H "${headers}" -d "{\"q\": \"$text\", \"source\": \"$in_lang\", \"target\": \"$out_lang\"}" "$translate_url" | awk -F ':' '{ print $2 }' | sed 's/"//g; s/}//g') 
			#printf '%s'"${result}\n"
			# result including up to 3 alternatives
			libreresponse=$(curl -s -X POST -H "${headers}" -d "{\"q\": \"$text\", \"source\": \"$in_lang\", \"target\": \"$out_lang\", \"alternatives\": 3}" "$translate_url")
			# seperate result & alternatives
			result=$(echo "$libreresponse" | jq -r '.translatedText')
			alternatives=$(echo "$libreresponse" | jq -r '.alternatives | join(", ")')
        	# copy to clipboard 
    	   	echo "$result" | wl-copy    		
}

speak_es(){
	DIR="$HOME/piper/models/es/"
	FILE1=es_MX-claude-high.onnx 
	FILE2=es_es_MX_claude_high_es_MX-claude-high.onnx.json

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2" ]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi

	# speak
	notify-send "Speaking" "$result"
	echo "$result" | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay -r 22050 -f S16_LE -t raw -
	show_translation
}

speak_en(){
	DIR="$HOME/piper/models/en/"
	FILE1="en_GB-alan-medium.onnx"
	FILE2="en_en_GB_alan_medium_en_GB-alan-medium.onnx.json"

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2" ]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi
	# speak
	notify-send "Speaking" "$result"
	echo "$result" | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay -r 22050 -f S16_LE -t raw -
	show_translation
}

speak_fi(){
	DIR="$HOME/piper/models/fi/"
	FILE1="fi_FI-harri-medium.onnx"
	FILE2="fi_fi_FI_harri_medium_fi_FI-harri-medium.onnx.json"

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2" ]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi

	# speak
	notify-send "Speaking" "$result"
	echo "$result" | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay -r 22050 -f S16_LE -t raw -
	show_translation
}

speak_de(){
	DIR="$HOME/piper/models/de/"
	FILE1="de_DE-thorsten-high.onnx"
	FILE2="de_de_DE_thorsten_high_de_DE-thorsten-high.onnx.json"

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2" ]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi

	# speak
	notify-send "Speaking" "$result"
	echo "$result" | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay -r 22050 -f S16_LE -t raw -
	show_translation
}

speak_fr(){
	DIR="$HOME/piper/models/fr/"
	FILE1="fr_FR-upmc-medium.onnx"
	FILE2="fr_fr_FR_upmc_medium_fr_FR-upmc-medium.onnx.json"

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2" ]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi

	# speak
	notify-send "Speaking" "$result"
	echo "$result" | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay -r 22050 -f S16_LE -t raw -
	show_translation
}

show_translation(){
    # Create options for dmenu
    options="\n
- More -\n- Speak -\n- Conversation -\n- Exit -"

	# show alternatives if they exist
	if [ -n "$alternatives" ]; then
    options="\n
- Alternatives -\n- More -\n- Speak -\n- Conversation -\n- Exit -"
	fi    
    # Combine the output with the options and pass it to dmenu
    selection=$(echo -e "$result $options" | /usr/bin/sxmo_dmenu.sh -i -p "$lang Translation")
    
    # Handle the user's selection
    case "$selection" in
        "- Alternatives -")
        	result="$alternatives"
            show_translation
            echo "$result" | wl-copy    		
            ;;    
        "- More -")
            gettext
            translate
            show_translation
            ;;
        "- Speak -")
			speak_check
			out_lang=$(echo "$lang" | cut -d' ' -f2 | tr -d '"')			
			speak_$out_lang
            ;;
        "- Conversation -")
			# Split the string into two parts using the hyphen as a delimiter.
			in_lang=$(echo "$lang" | cut -d' ' -f1 | cut -d'=' -f2 | tr -d '"')
			out_lang=$(echo "$lang" | cut -d' ' -f2 | tr -d '"')
			# Combine them in reverse order.
			flipped_lang="${out_lang} ${in_lang}"
			lang="$flipped_lang"
			gettext
			translate
			show_translation
            ;;
        "- Exit -")
            echo "Exiting..."
            exit 0
            ;;
        *)
            echo "Selected: $selection"
            ;;
    esac
}


requirements_check
check_docker
ask_languages
list_languages
gettext
translate
show_translation


exit 0
