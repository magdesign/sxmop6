#!/bin/sh
# title="$icon_rss wlan datausage"
# Author: magdesign
# License: MIT
# Description: Shows the wlan data usage
# Notes: v0.21 to reset, stop service and rm var/lib/vnstat/vnstat.db
# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# Check if requirement is installed
if command -v vnstat >/dev/null 2>&1 ;then
echo "vnstat installed"
else
  notify-send --urgency=critical "Please install vnstat"
  exit 1
fi

# Query
ASKWLAN="vnstat -d --short -i wlan0"
ASKGSM="vnstat -d --short -i qrtr0"

# Styles the output for mobile
STRIP="awk 'NR ==6 {print \$1, \$8, \$9} NR ==5 {print \$1, \$8, \$9} NR ==4 {print \$1, \$8, \$9}'"
WLAN="echo WLAN:"
GSM="echo GSM:"

# execute the commands
WLANUSAGE="$(eval $ASKWLAN | eval $STRIP)"
GSMUSAGE="$(eval $ASKGSM | eval $STRIP)"

# uncomment to display in a new terminal and close it in 5s
#sxmo_terminal.sh -- bash -c "${WLAN}&${WLANUSAGE}; exec bash" & PID=$!; sleep 5 && kill $PID

# display output with notify send
notify-send "WLAN Usage" "$WLANUSAGE"

exit 0
