# Custom sxmo userscripts, because we can.

**you need: `sudo apk add bash vnstat zxing tesseract-ocr tesseract-ocr-data-eng wl-clipboard imagemagick wf-recorder`**

Make each executable with: `sudo chmod +x`
For datausage display, first run the reset script.

Njoy and drop me a line on Masto:
https://fosstodon.org/@pocketvj

Get info [here](https://wiki.postmarketos.org/wiki/User:Magdesign)

## datausage_wlan.sh
shows how many MB is used.

make sure to set vnstat to run on boot:

`sudo apk add vnstat`

`sudo rc-update add vnstatd`

![screenshot](images/data_sml.png "datausage_wlan.sh")

## datausage_reset.sh
resets the counter to zero.

## empty_trash.sh
clears ~/.local/share/Trash/* and ~/.cache/*
then checks and notify-send if its emptied

## powerlevel.sh
display powerlevel in % and if its charging or discharging as notify-send popup.
on op6 it also shows charging amperes.

![screenshot of powerlevel](images/power_sml.png "powerlevel.sh")

## macchanger.sh
creates a random mac address on wlan adaptor.
(don't use anymore, instead modify  /etc/NetworkManager/conf.d/00-sxmo.conf and enable mac randomisation)

## gps_enable.sh
enables gps catcher.

## gyro.py
python bubble level (wasserwaage)

## ssh_toggler.sh
enables or disables ssh.

## ocr_roi.sh
capture text on screen and extracts it to the clipboard, also supports qr codes
and qr codes containing wifi passwords.

(if you want to start it via a gesture, you might have to add it to /usr/bin)

![screenshot](images/ocr_sml.png "ocr_roi.sh")

## qr_roi.sh
decode qr codes on screen and extracts it to the clipboard.
(out of date since ocr_roi.sh is better now)
![screenshot](images/qr_sml.png "qr_roi.sh")

## gif_roi.sh
selection screenrecording to gif
in sway.

## airplane.sh
toggle GSM modem state, ignoring wifi.

air![screenshot](images/airplane_sml.png "airplane.sh")

## screenrecorder.sh
records a selection of your screen to *.mp4 stop command is currently hardcoded to op6 volumebuttons instead of sxmo_inputhandler.sh
to do: fix stop button, add a maximum timeout.

## torch.sh
toggles the torch from On to both On to Off.

## sxmop6_yt_down.sh
Download video or audio from youtube.
![screenshot](images/yt_down.png "youtube downloader")

## sxmop6_adaptive_brightness.sh
automatically adjust screen brightness according to environment brightness

## sxmop6_libretranslator.sh
https://wiki.postmarketos.org/wiki/User:Magdesign#Offline_Translation

see https://odysee.com/@sharinggoodmoments:2/sxmop6_translate:3

## sxmop6_translator.sh
https://wiki.postmarketos.org/wiki/User:Magdesign#Offline_Translation

see https://odysee.com/@sharinggoodmoments:2/sxmop6_translate:3
