#!/bin/sh
# title="$icon_aud opus-converter"
# Author: magdesign
# Description: converts all audio files in folder to opus
# Notes: v0.2.1
# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
#. "/usr/bin/sxmo_common.sh"

# echeck for ffmpeg
if ! command -v ffmpeg &> /dev/null; then
    echo "Error: ffmpeg is not installed"
    exit 1
fi

# Loop through all files
for file in *; do
    # Check if the file is a regular file with supported audio formats
    case "$file" in
        *.flac|*.aiff|*.aif|*.mp3|*.wav|*.ogg|*.aac|*.m4a)
            # Get the filename without the extension
            filenameNoExt="${file%.*}"
            # Convert the audio file to opus with 128 kbps
            ffmpeg -i "$file" -c:a libopus -b:a 128k "$filenameNoExt.opus"
            ;;
    esac
done

# Remove all files ending with mp3, wav, ogg, aac, m4a
#find . -type f \( -name "*.mp3" -o -name "*.wav" -o -name "*.ogg" -o -name "*.aac" -o -name "*.m4a" \) -exec rm {} +


