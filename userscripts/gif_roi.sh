#!/bin/sh
# title="$icon_pst GIFscreen"
# Author: magdesign
# Description: records gif
# Requirements: bash imagemagick
# Notes:v0.3  Works only on sway/wayland
# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# to do: stop with volume button 
# cleanup the code mess!

# Check if ImageMagick is installed
if command -v magick >/dev/null 2>&1 ;then
echo "ImageMagick installed"
else
  notify-send --urgency=critical "Please install ImageMagick"
  exit 1
fi

# change only duration and framerate:
duration=5
total=$((6 * duration))
framerate=6
effective=$(echo "scale=3; 1 / $framerate" | bc)
# save resulting gif to:
location=~/Pictures
# tempdir to 
mkdir -p /tmp/gif/
DIR=/tmp/gif

sway_screenshot() {
	area="$(slurp -o)"
	if [ -z "$area" ]; then
		area="$(swaymsg -t get_outputs | jq -r '.[] |select (.focused) | .rect | "\(.x),\(.y) \(.width)x\(.height)"')"
	fi
	wxh=$(echo "$area" | cut -d " " -f 2)
	w=$(echo "$wxh" | cut -d "x" -f 1)
	h=$(echo "$wxh" | cut -d "x" -f 2)
	if [ -n "$wxh" ] && [ "$w" -gt 15 ] && [ "$h" -gt 15 ]; then
		#we have a selection (bigger than 15x15)
		notify-send -w -t 10000 "click me" "to start"
		# Capture continuous screenshots
		for i in $(seq 0 $((total - 1))); do
  		# Format the file name with leading zeros
  		filename=$(printf "%05d.png" "$i")
  		grim  -g "$area" "$DIR/$filename"
  		# Wait for 1/6th of a second
	  	sleep $effective
		done
	else
		notify-send "selection too small" "abort"
	exit 1
	fi
}

sway_screenshot

# message
notify-send -t 2000  "done..." "now converting"

# convert to animated gif
magick -delay $effective -loop 0 $DIR/*.png "$location/$(date +%Y-%m-%d-%H-%M-%S).gif"

notify-send --urgency=critical "done...see:" "$location"

# cleanup
rm -rf /tmp/gif/
exit 0
