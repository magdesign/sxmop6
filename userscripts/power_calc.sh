#!/bin/sh
# title="$icon_clc PowerCalculator"
# Description: Calculates Volt, Watt or Ampere
# Notes: v0.1

calculator_menu() {
	while true; do
		OPTIONS="$(cat <<-EOF
			Watt
			Volt
			Ampere
			
			Abort
		EOF
		)"
		
		OPTION=$(printf "%s" "$OPTIONS" | sxmo_dmenu.sh -p "Looking for:")

		case "$OPTION" in
			"Watt")
				voltage=$(echo "" | sxmo_dmenu.sh -p "Enter Voltage (Volt):")
				current=$(echo "" | sxmo_dmenu.sh -p "Enter Current (Ampere):")
				power=$(echo "$voltage * $current" | bc)
				notify-send -t 15000 "Power (Watt)" "$power W"
				;;

			"Volt")
				power=$(echo "" | sxmo_dmenu.sh -p "Enter Power (Watt):")
				current=$(echo "" | sxmo_dmenu.sh -p "Enter Current (Ampere):")
				voltage=$(echo "$power / $current" | bc)
				notify-send -t 15000 "Voltage (Volt)" "$voltage V"
				;;

			"Ampere")
				power=$(echo "" | sxmo_dmenu.sh -p "Enter Power (Watt):")
				voltage=$(echo "" | sxmo_dmenu.sh -p "Enter Voltage (Volt):")
				current=$(echo "$power / $voltage" | bc)
				notify-send -t 15000 "Current (Ampere)" "$current A"
				;;

			*)
				exit 0
				;;
		esac
	done
}

# If script is executed without argument, open menu
if [ -z "$1" ]; then
	set -- calculator_menu
fi

"$@"
