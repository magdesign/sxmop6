#!/bin/sh
# title="$icon_ytb YT-Downloader"
# Description: Download YT Audio/Video 
# Author: magdesign
# Notes: v0.4
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# todo: error reporting
# check if file is really downloaded before notify-send

# check for dependencies
if ! command -v yt-dlp >/dev/null 2>&1; then
    notify-send "Error: no yt-dlp installed." "read: pmos magdesign wiki"
    exit 1
fi

menu() {
	MENUINPUT="$(sxmo_dmenu.sh -p "Select" <<EOF
YT-Audio-opus
MIX-Audio-opus

YT-Video-720
YT-Video-1080

Abort
EOF
	)" || exit
	case "$MENUINPUT" in
		"YT-Audio-opus")
			yt-audio-opus
			exit 0
			;;
		"MIX-Audio-opus")
			mix-audio-opus
			exit 0
			;;
		"YT-Video-720")
			yt-video-720
			exit 0
			;;
		"YT-Video-1080")
			yt-video-720
			exit 0
			;;
		"Abort")
			exit 0
			;;
		*)
			# wrong/no input
			echo "no selection"
			;;
	esac
}

yt-audio-opus() {
while true; do #for retry loop
    entry=$(echo -e "Abort
" | /usr/bin/sxmo_dmenu.sh -p "Youtube URL")

    if [ "$entry" = "Abort" ]; then
		break
	fi

    if [ -n "$entry" ]; then
        # Check if entry contains 'www.you'
        if echo "$entry" | grep -q 'www\.you'; then
            notify-send -u low -t 3000 "Converting to opus" "$entry"
            /usr/bin/sxmo_terminal.sh /usr/bin/yt-dlp --extract-audio --audio-format opus -o '~/Music/%(title)s.%(ext)s' "$entry"
			notify-send -u low "finished!" "stored in ~/Music"
            break
        else
            choice=$(echo -e "Retry
Abort" | /usr/bin/sxmo_dmenu.sh -p "Invalid URL")
            if [ "$choice" = "Abort" ]; then
                break # Exit
            fi
        fi
	else
        #exit
        break
	fi
done
}

mix-audio-opus() {
while true; do #for retry loop
    entry=$(echo -e "Abort
" | /usr/bin/sxmo_dmenu.sh -p "Mixcloud URL")

    if [ "$entry" = "Abort" ]; then
		break
	fi

    if [ -n "$entry" ]; then
        # Check if entry contains 'www.mix'
        if echo "$entry" | grep -q 'www\.mix'; then
            notify-send -u low -t 3000 "Converting to opus" "$entry"
            /usr/bin/sxmo_terminal.sh /usr/bin/yt-dlp --extract-audio --audio-format opus -o '~/Music/%(title)s.%(ext)s' "$entry"
			notify-send -u low "finished!" "stored in ~/Music"
            break
        else
            choice=$(echo -e "Retry
Abort" | /usr/bin/sxmo_dmenu.sh -p "Invalid URL")
            if [ "$choice" = "Abort" ]; then
                break # Exit
            fi
        fi
	else
        #exit
        break
	fi
done
}

yt-video-720() {

while true; do #for retry loop
    entry=$(echo -e "Abort
" | /usr/bin/sxmo_dmenu.sh -p "Youtube URL")
    if [ "$entry" = "Abort" ]; then
		break
	fi

    if [ -n "$entry" ]; then
        # Check if entry contains 'www.you'
        if echo "$entry" | grep -q 'www\.you'; then
            notify-send -u low -t 3000 "Downloading" "$entry"
 			/usr/bin/sxmo_terminal.sh /usr/bin/yt-dlp -f "bestvideo[height<=720]+bestaudio" -o '~/Videos/%(title)s.%(ext)s' --merge-output-format mp4 "$entry"
			notify-send -u low "finished!" "stored in ~/Videos"
            break
        else
            choice=$(echo -e "Retry
Abort" | /usr/bin/sxmo_dmenu.sh -p "Invalid URL")
            if [ "$choice" = "Abort" ]; then
                break # Exit
            fi
        fi
	else
        #exit
        break
	fi
done
}

yt-video-1080() {
while true; do #for retry loop
    entry=$(echo -e "Abort
" | /usr/bin/sxmo_dmenu.sh -p "Youtube URL")
    if [ "$entry" = "Abort" ]; then
		break
	fi

    if [ -n "$entry" ]; then
        # Check if entry contains 'www.you'
        if echo "$entry" | grep -q 'www\.you'; then
            notify-send -u low -t 3000 "Downloading" "$entry"
 			/usr/bin/sxmo_terminal.sh /usr/bin/yt-dlp -f "bestvideo[height<=1080]+bestaudio" -o '~/Videos/%(title)s.%(ext)s' --merge-output-format mp4 "$entry"
			notify-send -u low "finished!" "stored in ~/Videos"
            break
        else
            choice=$(echo -e "Retry
Abort" | /usr/bin/sxmo_dmenu.sh -p "Invalid URL")
            if [ "$choice" = "Abort" ]; then
                break # Exit
            fi
        fi
	else
        #exit
        break
	fi
done
}


if [ $# -gt 0 ]
then
	"$@"
else
	menu
fi
