#!/bin/sh
# title="icon_spk PocketHeater"
# Author: magdesign
# Description: heatup the phonr
# Notes: v0.1alpha  requires: glmark2 stress-ng
# check requirements

if command -v stress-ng &> /dev/null
then
    echo "stress-ng is installed"
else
  notify-send --urgency=critical "install stress-ng"
  exit 1
fi

if command -v glmark2 &> /dev/null
then
    echo "glmark2 is installed"
else
  notify-send --urgency=critical "install glmark2"
  exit 1
fi


# to do add a toggler
# execute the stupid things
glmark2 -b periforms &

stress-ng --cpu 0 --cpu-method all --matrix 5 --vm 4 --vm-bytes 2G --hdd 4 --hdd-bytes 4G &

# to do: add control / adjust temperature
~/sxmop6/userscripts/temperaturewatcher.sh 


