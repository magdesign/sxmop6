#!/bin/bash
# title="$icon_tof SSH enable"
# Description: Toggles ssh on or off
# Author: magdesign
# Note: v0.2 
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

path_of_script="$(cd "$(dirname "$0")"; pwd)"
echo $path_of_script
# check the sshd status
service_status=$(rc-service sshd status)

if [[ $service_status == *"started"* ]]; then
    echo "Service sshd is started. Stopping now..."
    # asking for privileges and disabling ssh
    sxmo_terminal.sh -- sh -c 'sudo rc-service sshd stop && sudo rc-update del sshd'
    # notify that service is stopping
    notify-send "SSH stopped"
     # change the toggle symbol and name of this script
    sed -i 's/^# title="\$icon_ton SSH disable"/# title="\$icon_tof SSH enable"/' $path_of_script'/ssh_toggle.sh'

elif [[ $service_status == *"stopped"* ]]; then
    echo "Service sshd is stopped. Starting now..."
    # asking for privileges and enabling ssh
    sxmo_terminal.sh -- sh -c 'sudo rc-service sshd start && sudo rc-update add sshd'
    # notify that it is starting
    notify-send "SSH started"
    # change the toggle symbol and name of this script
    sed -i 's/^# title="\$icon_tof SSH enable"/# title="\$icon_ton SSH disable"/' $path_of_script'/ssh_toggle.sh'

else
    notify-send --urgency=critical "Error: no status"
fi
