#!/bin/sh
# title="$icon_state_proximity BubbleLevel"
# Description: bubble level, wasserwaage
# Notes: v0.1

# Run Python code directly within the shell script
python3 <<EOF
# Start of Python code
import tkinter as tk
import subprocess
import math
import threading
import atexit

# Global variable to store the ssccli process
ssccli_process = None

# Function to clean up the ssccli process
def cleanup():
    global ssccli_process
    if ssccli_process:
        ssccli_process.terminate()
        ssccli_process.wait()
        print("ssccli process terminated.")

# Register the cleanup function
atexit.register(cleanup)

# Kalman filter parameters
kalman_gain = 0.1
kalman_x = 0
kalman_y = 0

# Smoothing filter parameters
alpha = 0.05
filtered_degX = 0
filtered_degY = 0
filtered_degZ = 0

# Create the main window
root = tk.Tk()
root.title("Red Circle and Green Circles")
root.attributes("-fullscreen", True)  # Make the window fullscreen

# Create a canvas with a black background
canvas = tk.Canvas(root, bg="black")
canvas.pack(fill=tk.BOTH, expand=True)

# Get the screen dimensions
window_width = root.winfo_screenwidth()
window_height = root.winfo_screenheight()

# Draw the red circle (on the lowest layer)
circle_radius = 10
red_circle = canvas.create_oval(
    window_width // 2 - circle_radius,
    window_height // 2 - circle_radius,
    window_width // 2 + circle_radius,
    window_height // 2 + circle_radius,
    outline="red",
    width=2
)

# Draw the stationary green circles (on top of the red circle)
small_radius = 10  # Small green circle radius
big_radius = 30    # Big green circle radius

green_circle1 = canvas.create_oval(
    window_width // 2 - small_radius,
    window_height // 2 - small_radius,
    window_width // 2 + small_radius,
    window_height // 2 + small_radius,
    outline="green",
    width=1
)
green_circle2 = canvas.create_oval(
    window_width // 2 - big_radius,
    window_height // 2 - big_radius,
    window_width // 2 + big_radius,
    window_height // 2 + big_radius,
    outline="green",
    width=1
)

# Text labels to display degrees
degrees_label_x = canvas.create_text(
    10, 10, anchor="nw", fill="white", font=("Arial", 18), text="X: 0.0°"
)
degrees_label_y = canvas.create_text(
    10, 30, anchor="nw", fill="white", font=("Arial", 18), text="Y: 0.0°"
)
degrees_label_z = canvas.create_text(
    10, 50, anchor="nw", fill="white", font=("Arial", 18), text="Z: 0.0°"
)

# Add a threshold for "reaching the center"
threshold = 1  # Adjust this value as needed

# Function to check if the red circle is at the center
def is_at_center(red_coords):
    red_center_x = (red_coords[0] + red_coords[2]) / 2
    red_center_y = (red_coords[1] + red_coords[3]) / 2
    center_x = window_width // 2
    center_y = window_height // 2
    return abs(red_center_x - center_x) < threshold and abs(red_center_y - center_y) < threshold

# Function to check if the red circle enters a green circle
def check_collision():
    # Get the coordinates of the red circle
    red_coords = canvas.coords(red_circle)
    red_center_x = (red_coords[0] + red_coords[2]) / 2
    red_center_y = (red_coords[1] + red_coords[3]) / 2

    # Check collision with green_circle1 (small circle)
    green1_coords = canvas.coords(green_circle1)
    green1_center_x = (green1_coords[0] + green1_coords[2]) / 2
    green1_center_y = (green1_coords[1] + green1_coords[3]) / 2
    distance1 = math.sqrt((red_center_x - green1_center_x) ** 2 + (red_center_y - green1_center_y) ** 2)
    if distance1 <= small_radius + circle_radius:
        canvas.itemconfig(green_circle1, outline="pink")
    else:
        canvas.itemconfig(green_circle1, outline="green")

    # Check collision with green_circle2 (big circle)
    green2_coords = canvas.coords(green_circle2)
    green2_center_x = (green2_coords[0] + green2_coords[2]) / 2
    green2_center_y = (green2_coords[1] + green2_coords[3]) / 2
    distance2 = math.sqrt((red_center_x - green2_center_x) ** 2 + (red_center_y - green2_center_y) ** 2)
    if distance2 <= big_radius + circle_radius:
        canvas.itemconfig(green_circle2, outline="pink")
    else:
        canvas.itemconfig(green_circle2, outline="green")

# Draw the green crosshair (on top of the red circle)
crosshair_horizontal = canvas.create_line(
    0, window_height // 2, window_width, window_height // 2, fill="green", width=1
)
crosshair_vertical = canvas.create_line(
    window_width // 2, 0, window_width // 2, window_height, fill="green", width=1
)

# Function to update the circle position
def update_circle():
    global kalman_x, kalman_y, filtered_degX, filtered_degY, filtered_degZ, ssccli_process

    # Execute the bash command and capture the output
    ssccli_process = subprocess.Popen(
        "ssccli --sensor accelerometer --timeout -1",
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True
    )

    # Read the output line by line
    for line in ssccli_process.stdout:
        # Remove the prefix "Accelerometer sensor measurement:"
        data = line.strip().replace("Accelerometer sensor measurement: ", "")

        # Split the data into components
        parts = data.split()

        # Extract valueX, valueY, and valueZ from the line
        try:
            valueX = float(parts[0].split('=')[1].rstrip(','))  # Extract X value
            valueY = float(parts[1].split('=')[1].rstrip(','))  # Extract Y value
            valueZ = float(parts[2].split('=')[1])  # Extract Z value
        except (IndexError, ValueError) as e:
            print(f"Error parsing line: {line.strip()}")
            continue

        # Calculate degrees
        degX = math.degrees(math.atan2(valueX, valueZ))
        degY = math.degrees(math.atan2(valueY, valueZ))
        degZ = math.degrees(math.atan2(math.sqrt(valueX**2 + valueY**2), valueZ))

        # Apply filtering
        filtered_degX = alpha * degX + (1 - alpha) * filtered_degX
        filtered_degY = alpha * degY + (1 - alpha) * filtered_degY
        filtered_degZ = alpha * degZ + (1 - alpha) * filtered_degZ

        # Update the degrees display
        canvas.itemconfig(degrees_label_x, text=f"X: {filtered_degX:.1f}°")
        canvas.itemconfig(degrees_label_y, text=f"Y: {filtered_degY:.1f}°")
        canvas.itemconfig(degrees_label_z, text=f"Z: {filtered_degZ:.1f}°")

        # Map the values to the window coordinates
        new_x = int(filtered_degX / 90 * (window_width // 2 - circle_radius) + window_width // 2)
        new_y = int(-filtered_degY / 90 * (window_height // 2 - circle_radius) + window_height // 2)

        # Apply Kalman filter
        kalman_x = kalman_x + kalman_gain * (new_x - kalman_x)
        kalman_y = kalman_y + kalman_gain * (new_y - kalman_y)

        # Move the red circle
        canvas.coords(
            red_circle,
            kalman_x - circle_radius,
            kalman_y - circle_radius,
            kalman_x + circle_radius,
            kalman_y + circle_radius
        )

        # Ensure the red circle is on the lowest layer
        canvas.lower(red_circle)

        # Check if the red circle is at the center
        red_coords = canvas.coords(red_circle)
        if is_at_center(red_coords):
            canvas.itemconfig(red_circle, fill="red")
        else:
            canvas.itemconfig(red_circle, fill="")

        # Check for collisions
        check_collision()

        # Update the canvas
        root.update()

# Function to start the update loop in a separate thread
def start_update_loop():
    update_thread = threading.Thread(target=update_circle, daemon=True)
    update_thread.start()

# Add a Quit button
quit_button = tk.Button(root, text="Quit", command=root.destroy, bg="red", fg="white", font=("Arial", 16))
quit_button.place(relx=1.0, rely=0.0, anchor="ne", x=-10, y=10)  # Position in the top-right corner

# Start the update loop in a separate thread
start_update_loop()

# Run the application
root.mainloop()
# End of Python code
EOF

# Continue with other shell commands
echo "Python program executed successfully."
