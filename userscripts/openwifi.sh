#!/bin/sh
# title="$icon_wifi_signal_3 openWifi"
# Author: magdesign
# Description: list open wifi
# Notes: v0.1
# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# check if wifi is enabled
if  [ $(nmcli radio wifi) == "disabled" ]; then
	notify-send -u critical "Enable Wifi first!" "exited"
	exit 1
else
	WIFI=$(nmcli -f SSID,SECURITY,BARS dev wifi | tail -n +2 | grep -v "WPA" | awk '!seen[$0]++' | sed '/^--/d' | sed 's/ -- //g')
	echo $WIFI #| tr ' ' '\n' 
	notify-send "open " "$WIFI" 
fi

exit 0	
