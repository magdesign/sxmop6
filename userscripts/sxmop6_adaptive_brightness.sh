#!/bin/sh
# Description: adaptive brightness of screen
# Notes: v0.3 
# include common definitions
# shellcheck source=scripts/core/sxmo_common.sh
#. sxmo_common.sh

# should we  maybe check if iio-sensors is installed?
# add toggle: write to $XDG_CACHE_HOME"/sxmo/adaptive.on
# 

# check if iio-sensor is running
if [[ $(rc-service iio-sensor-proxy status | awk '{print $3}') == "stopped" ]]; then
	echo "starting iio-sensor"
	sxmo_terminal.sh -- sh -c 'doas rc-service iio-sensor-proxy start'
else
	echo "iio-sensor already running"
fi


NIGHT=2 # 
LOW=16 # breakpoint to stay on lowest value
INDOOR=7000
OUTDOOR=20000 # sunlight go 100%
BRGHT=$(brightnessctl get)
TIMEOUT=5
# Initialize
count=0  
sum=0

# Monitor sensor
monitor-sensor --light | while read -r line; do
  value=$(echo "${line}" | awk '{print $3+0}')
  echo "$value" #enable for debugging

# Check if screen is on
  if swaymsg -t get_outputs --raw | grep -q '"power": true,'; then
    echo "Screen is on"
    # You can add logic here to do something with the $value if needed
  else
    echo "Screen is off"
    while true; do
      if swaymsg -t get_outputs --raw | grep -q '"power": true,'; then
        echo "Screen back on. Resuming monitoring."
        break
      else
        echo "Screen is still off. Sleeping for $TIMEOUT seconds."
        sleep "$TIMEOUT"
      fi
    done
fi 

  sum=$((sum + value))  # Accumulate the sum
  count=$((count + 1))  # Increment the counter
  
  # Check if the counter has reached 10
  if [ "$count" -ge 10 ]; then
    echo 'We should do math now'
    echo "The sum is: $sum"
    
    # Calculate 
    if [ "$count" -gt 0 ]; then
#      average=$(echo "scale=2; $sum / $count" | bc)   
      average=$(echo "scale=2; $sum / 7" | bc)
      echo "Average: $average"
	LIGHT=$(echo "$average" | awk '{print int($1 + 0.5)}')
	echo "$LIGHT"
    fi
    

if [ "$LIGHT" -le "$NIGHT" ]; then 
	brightnessctl set 2
	echo "set night"
	sleep $TIMEOUT
		
elif [ "$LIGHT" -le "$LOW" ]; then 
	brightnessctl set 15
	sleep $TIMEOUT

	
elif [ "$LIGHT" -le "$INDOOR" ]; then 
	brightnessctl set 500
	sleep $TIMEOUT


elif [ "$LIGHT" -le "$OUTDOOR" ]; then 
	brightnessctl set 900
	sleep $TIMEOUT
	
elif [ "$LIGHT" -gt "$OUTDOOR" ]; then 
	# invert color if gamma is available
	brightnessctl set 1023
	sleep $TIMEOUT
fi

sum=0
count=0
fi

done

exit 0
