#!/bin/sh
# title="$icon_usr apertiumTranslator"
# Description: Translates text in wofi/bmenu
# Author: magdesign
# Notes: v0.3 fixed some typos, but needs a refactoring
# translations are automatically saved in clipboard to paste in any application

requirements_check(){
	if ! command -v apertium >/dev/null 2>&1; then
	    notify-send "Error: apertium is not installed."
    exit 1
fi
}

speak_check(){
# check for speak-engine
if ! command -v piper >/dev/null 2>&1; then
    notify-send "installed piper-tts & py3-piper-tts"    
	exit 1
fi
}

listlanguages() {
    options="$(apertium -l) 
\n- Abort -"
    
    lang=$(echo -e "$options" | /usr/bin/sxmo_dmenu.sh -p "Enter text")

    # Handle user's selection
    case "$lang" in
        "- Abort -")
            exit 0
            ;;
        *)
          #  echo "No valid option selected."
            ;;
    esac
}

gettext() {
    options="\n- Abort -"
    
    text=$(echo -e "$options" | /usr/bin/sxmo_dmenu.sh -p "Enter text")
    echo "$text" > /tmp/text
    case "$text" in
        "- Abort -")
            exit 0
            ;;
        *)
          #  echo "No valid option selected."
            ;;
    esac
}

translate() {
    if [ -n "$text" ]; then
        # notify-send "Translating '$text' to '$lang'"
        path="/tmp/text"
        output="$(apertium $lang $path)"
        # copy to clipboard 
        echo "$output" | wl-copy
    fi
}


# remove words it dont know marked with * (need an exeption library, like linux, usb, names, etc....)
clean_speak(){
	cleaned=$(echo "$output" | sed 's/\*[^ ]*//g' | sed 's/  / /g' | sed 's/^ //;s/ $//')
}

speak_spa(){
	DIR="$HOME/piper/models/es/"
	FILE1=es_MX-claude-high.onnx 
	FILE2=es_es_MX_claude_high_es_MX-claude-high.onnx.json

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2"]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi

	# speak
	echo "$cleaned" | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay -r 22050 -f S16_LE -t raw -
	show_translation
}


speak_eng(){
	DIR="$HOME/piper/models/en/"
	FILE1="en_GB-alan-medium.onnx"
	FILE2="en_en_GB_alan_medium_en_GB-alan-medium.onnx.json"

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2"]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi
	# speak
	echo "$cleaned" | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay -r 22050 -f S16_LE -t raw -
	show_translation
}


speak_fin(){
	DIR="$HOME/piper/models/fi/"
	FILE1="fi_FI-harri-medium.onnx"
	FILE2="fi_fi_FI_harri_medium_fi_FI-harri-medium.onnx.json"

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2"]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi

	# speak
	echo "$cleaned" | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay -r 22050 -f S16_LE -t raw -
	show_translation
}

speak_deu(){
	DIR="$HOME/piper/models/de/"
	FILE1="de_DE-thorsten-high.onnx"
	FILE2="de_de_DE_thorsten_high_de_DE-thorsten-high.onnx.json"

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2"]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi

	# speak
	echo "$cleaned" | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay -r 22050 -f S16_LE -t raw -
	show_translation
}

speak_fra(){
	DIR="$HOME/piper/models/fr/"
	FILE1="fr_FR-upmc-medium.onnx"
	FILE2="fr_fr_FR_upmc_medium_fr_FR-upmc-medium.onnx.json"

	if [ ! -d "$DIR" ] || [ ! -f "$DIR$FILE1" ] || [ ! -f "$DIR$FILE2"]; then
    	notify-send "$DIR" "Install language"
		exit 1
	fi

	# speak
	echo "$cleaned" | /usr/bin/piper --model $DIR$FILE1 --config $DIR$FILE2 --output-raw | aplay -r 22050 -f S16_LE -t raw -
	show_translation
}

show_translation(){
    # Create options for dmenu
    options="\n
- More -\n- Speak -\n- Conversation -\n- Exit -"
    
    # Combine the output with the options and pass it to dmenu
    selection=$(echo -e "$output $options" | /usr/bin/sxmo_dmenu.sh -i -p "$lang Translation")
    
    # Handle the user's selection
    case "$selection" in
        "- More -")
            gettext
            translate
            show_translation
            ;;
        "- Speak -")
			speak_check
			clean_speak
			out_lang=$(echo "$lang" | cut -d'-' -f2 | tr -d '"')			
			speak_$out_lang
            ;;
        "- Conversation -")
			# Split the string into two parts using the hyphen as a delimiter.
			in_lang=$(echo "$lang" | cut -d'-' -f1 | cut -d'=' -f2 | tr -d '"')
			out_lang=$(echo "$lang" | cut -d'-' -f2 | tr -d '"')
			# Combine them in reverse order.
			flipped_lang="${out_lang}-${in_lang}"
			lang="$flipped_lang"
			gettext
			translate
			show_translation
            ;;
        "- Exit -")
            echo "Exiting..."
            exit 0
            ;;
        *)
            echo "Selected: $selection"
            ;;
    esac
}

# Run
requirements_check
listlanguages
gettext
translate
show_translation

exit 0
