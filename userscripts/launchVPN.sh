#!/bin/sh
# title="$icon_wifi_key_signal_4 openVPN"
# Author: magdesign
# Description: openVPN connector
# Notes: v0.2

# currently does not reach check_vpn
# but its working so far
# configs in 'vpn' folder in 'Downloads' or $HOME

# todo: check if already running

dependency_check(){
	if ! command -v openvpn >/dev/null 2>&1; then
	    notify-send "please install openvpn"
    	exit 1
	fi
}

select_config() {
    options="
Documents/vpn

Downloads/vpn

Home/vpn

- Abort -"
    config=$(echo -e "$options" | /usr/bin/sxmo_dmenu.sh -p "Select Config")
    case "$config" in
        "- Abort -")
            exit 0
            ;;
        "Documents/vpn")
		options=$(ls -1 "$HOME/Documents/vpn")
	    config=$(echo -e "$options" | /usr/bin/sxmo_dmenu.sh -p "Select Config")
		config="$HOME/Documents/vpn/$config"
		notify-send -t 2000 "$config"
            ;;
        "Downloads/vpn")
		options=$(ls -1 "$HOME/Downloads/vpn")
   		config=$(echo -e "$options" | /usr/bin/sxmo_dmenu.sh -p "Select Config")
		config="$HOME/Downloads/vpn/$config"
		notify-send -t 2000 "$config"
            ;;
        "Home/vpn")
		options=$(ls -1 "$HOME/vpn")
   		config=$(echo -e "$options" | /usr/bin/sxmo_dmenu.sh -p "Select Config")
		config="$HOME/vpn/$config"
		notify-send -t 2000 "$config"
            ;;
        *)
            ;;
    esac
    echo "$config"
}

get_user() {
    options="\nvpnbook
\n- Abort -"
    username=$(echo -e "$options" | /usr/bin/sxmo_dmenu.sh -p "Enter username")
    echo "$username"
    case "$username" in
        "- Abort -")
            exit 0
            ;;
        *)
          #  echo "No valid option selected."
            ;;
    esac
}

get_pass() {
    options="\nhu86c9k
\n- Abort -"
    pass=$(echo -e "$options" | /usr/bin/sxmo_dmenu.sh -p "Enter password")
    echo "$pass"
    case "$pass" in
        "- Abort -")
            exit 0
            ;;
        *)
          #  echo "No valid option selected."
            ;;
    esac
}

write_credentials(){

	echo "$username" > /tmp/vpn 
	echo "$pass" >> /tmp/vpn 
}

enable_vpn(){
		sxmo_terminal.sh -- sh -c "doas openvpn --config '$config' --auth-user-pass /tmp/vpn" 
		rm /tmp/vpn
}

check_vpn(){
	if ifconfig | grep -q tun* ; then 
		notify-send "vpn on"
	else
		notify-send -u critical "error, no vpn"
	fi
}

dependency_check
select_config
get_user
get_pass
write_credentials
enable_vpn &
sleep 22
check_vpn
rm /tmp/vpn

exit 0


