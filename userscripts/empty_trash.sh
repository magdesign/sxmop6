# !/bin/sh
# title="$icon_trh emptyTrash"
# Author: magdesign
# Note: v0.2

# delete trash
rm -rf ~/.local/share/Trash/*

# delete cache
rm -rf ~/.cache/*


# check if trash is empty
if [ -z "$(ls -A ~/.local/share/Trash)" ]; then
    notify-send -u low "Trash is empty"
else
	notify-send -u low "there is still Trash!"
fi

exit 0


# flatpak
#sudo rm -rfv /var/tmp/flatpak-cache-*
#sudo flatpak uninstall --unused -y
