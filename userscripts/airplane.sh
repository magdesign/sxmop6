#!/bin/sh
# title="$icon_ton Modem disable"
# Author: Zerwuerfnis.org / magdesign
# License GPL3+
# Note: v0.2 removed wifi disabling
# shellcheck source=scripts/core/sxmo_common.sh
. "/usr/bin/sxmo_common.sh"

# path of this script, to toggle name & icon
path_of_script=~/.config/sxmo/userscripts/airplane.sh    

#checks gsm modem state    
radio="$(nmcli radio all | awk 'FNR == 2 {print $4}')"
#checks wifi modem state
radiowifi="$(nmcli radio all | awk 'FNR == 2 {print $2}')"

if [ "$radio" == "enabled" ]

    then
        nmcli radio wwan off
        #nmcli radio all off
        notify-send "Modem GSM off" "WIFI $radiowifi"
	sed -i 's/^# title="\$icon_ton Modem disable"/# title="\$icon_tof Modem enable"/' $path_of_script

	else
        nmcli radio wwan on
        #nmcli radio wwan on
        notify-send "Modem GSM on" "WIFI $radiowifi"
	sed -i 's/^# title="\$icon_tof Modem enable"/# title="\$icon_ton Modem disable"/' $path_of_script
        echo turning on
fi
