#!/bin/sh
# title="$icon_inf Temperature"
# Author: magdesign
# License: GPL3+
# Note: v0.1 tested obly on OP6, Show current temp in an intervall of 2s and show top command creating heat when value exceeds
# Todo: pause the command until heat reaches a certain value

LIMIT=77

while true; do

TEMP=$(cat /sys/class/thermal/thermal_zone*/temp | awk '{sum += $1} END {printf("%d", sum/NR/1000)}')

HEATER=$(ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%cpu | awk 'NR==2 {print $3}')

if [ $TEMP -gt $LIMIT ]; then
 clear
  printf "%s\r" "$TEMP °C lets pause: $HEATER" 
else
clear
  printf "%s\r" "$TEMP"°C
fi
 sleep 2
done
