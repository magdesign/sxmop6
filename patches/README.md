# Patches

## layout.mini.h
layout to compile mini keyboard (compiled version in bin)

## layout.mobintl.h
layout to compile keyboard with missing characters and other emojis (compiled version in bin)

see https://wiki.postmarketos.org/wiki/User:Magdesign#Keyboard_Modifications
and
https://github.com/jjsullivan5196/wvkbd 
