# sxmop6

useful tweaks for op6/pocof1 sdm845 phones based on [postmarketos](https://postmarketos.org) and [sxmo](https://sxmo.org)

there are some writeups in the [pmos wiki](https://wiki.postmarketos.org/wiki/User:Magdesign)

## Why

because we can, its foss ❤️
and I need a place to store my customisations...

# What is it

useful tweaks for the ui, menu and system. including tools focusing on offline usage. 🚴‍♂️

# Install

clone this repo to your home folder

```
cd ~/

git clone https://codeberg.org/magdesign/sxmop6.git

cd sxmop6

chmod +x deploy_sxmop6.sh

./deploy_sxmop6.sh
```

there are some useful scripts in ```userscripts``` ```launchers``` and ```bin```

check the customized keyboard to trigger mouse and speech function, dont forget to copy the ```config/sway``` file.

ask when you need help!

# Background

overwrite /usr/share/wallpapers/postmarketos.jpg

# Licenses
[TouchpadEmulator](https://gitlab.com/CalcProgrammer1/TouchpadEmulator)
[Wvkbd](https://github.com/jjsullivan5196/wvkbd)
[sxmo](https://git.sr.ht/~mil/sxmo-utils)

